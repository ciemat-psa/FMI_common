#ifndef INTEGRATOR_H
#define INTEGRATOR_H

#include <QDialog>
#include <QAbstractButton>
#include "import/integrators/include/IntegratorType.h"
#include "opt_project.h"

namespace Ui {
class integrator;
}

class integrator : public QDialog
{
    Q_OBJECT

public:
    explicit integrator(QWidget *parent = 0);
    void     populateIntegratorInfo();
    void     setIntInfo(opt_sim sim);
    int      getIntegratorIndex(IntegratorType key);
    opt_sim  getIntInfo();
    void     deleteStyleSheet();
    void     hideDefaultsButtons();
    ~integrator();

    // Integrator info
    QStringList           int_name;
    QStringList           int_des;
    QList<IntegratorType> int_key;
    QList<bool>           int_step;
    QList<bool>           int_order;
    QList<bool>           int_tols;

    QString company;
    QString appName;

private slots:
    void on_cbIntegrator_currentIndexChanged(int index);
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::integrator *ui;
};

#endif // INTEGRATOR_H
