#include "penconfig.h"
#include "ui_penconfig.h"
#include "commonapp.h"

penConfig::penConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::penConfig)
{
    ui->setupUi(this);
}

penConfig::~penConfig()
{
    delete ui;
}

void penConfig::deleteSytyleSheet()
{
    ui->lTitle->setStyleSheet(sEmpty);
}

QPen penConfig::getPen()
{
    // Color -- already set
    // Width -- already set
    // Style -- already set

    return pen;
}

void penConfig::setPen(QPen p)
{
    pen = p;

    // Color
    setBackgroundColor(ui->leColor,pen.color());
    // Width
    ui->sbWidth->setValue(pen.width());
    // Style
    setLineStyleCombo(ui->cbLine,pen);
}


void penConfig::on_sbWidth_valueChanged(int arg1)
{
    pen.setWidth(arg1);
    setLineStyleCombo(ui->cbLine,pen);
}

void penConfig::on_tbColor_clicked()
{
    dColor.setCurrentColor(pen.color());
    dColor.setWindowIcon(windowIcon());
    if (dColor.exec())
    {
        pen.setColor(dColor.currentColor());
        setBackgroundColor(ui->leColor,pen.color());
        setLineStyleCombo(ui->cbLine,pen);
    }
}

void penConfig::on_cbLine_currentIndexChanged(int index)
{
    pen.setStyle((Qt::PenStyle)index);
}
