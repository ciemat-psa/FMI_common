/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef VIDEOCONF_H
#define VIDEOCONF_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class VideoConf;
}

class VideoConf : public QDialog
{
    Q_OBJECT

public:
    explicit VideoConf(QWidget *parent = 0);
    ~VideoConf();
    void setData(double time, int frames, int fps);
    int getFPS();
    void setFPS(int value);
    double getMultiplier();
    void setMultiplier(double value);
    void setState();
    QString getFilename() const;
    bool getSaveToFile() const;
    void setSaveToFile(bool value);
    double getTotalFrames();
    bool validState();

private slots:
    void on_sbFPS_valueChanged(const QString &arg1);
    void on_buttonBox_clicked(QAbstractButton *button);
    void on_rbReal_clicked();
    void on_rbSlow_clicked();
    void on_rbFast_clicked();
    void on_sbSlow_valueChanged(const QString &arg1);
    void on_sbFast_valueChanged(const QString &arg1);

private:
    Ui::VideoConf *ui;
    double t;
    double m;
    int    f;
    QString filename;
    bool   saveToFile=true;
};

#endif // VIDEOCONF_H
