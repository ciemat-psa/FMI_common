/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "qtabwidgetext.h"
#include<QTabBar>
#include<QAction>
#include<QToolButton>

const QString PRO_WIDGET = "PRO_WIDGET";
const QString PRO_TAB    = "PRO_TAB";
const QString PRO_ICON   = "PRO_ICON";
const QString PRO_TEXT   = "PRO_TEXT";
const QString PRO_WIN    = "PRO_WIN";

QTabWidgetExt::QTabWidgetExt(QWidget *parent) : QTabWidget(parent)
{
}

QTabWidgetExt::~QTabWidgetExt(){
    qDeleteAll(rw);
    // qDeleteAll(bOpen); -- Deleted in tabBar
}

void QTabWidgetExt::openTriggered()
{
    QToolButton *b = (QToolButton *)sender();

    if (b != NULL)
    {
        resultWin *rw = b->property(PRO_WIN.toStdString().c_str()).value<resultWin *>();
        QWidget *w    = b->property(PRO_TAB.toStdString().c_str()).value<QWidget *>();
        QIcon   i     = b->property(PRO_ICON.toStdString().c_str()).value<QIcon>();
        QString t     = b->property(PRO_TEXT.toStdString().c_str()).toString();
        QAction *act  = w->property(PRO_WIDGET.toStdString().c_str()).value<QAction *>();

        if (act!=NULL) act->setChecked(false);
        rw->setResult(w,i,t);
        rw->show();
    }
}

int QTabWidgetExt::addTab(QWidget *widget, const QString &label)
{
    return QTabWidget::addTab(widget,label);
}

int QTabWidgetExt::addTab(QWidget *widget, const QIcon& icon, const QString &label)
{
    return QTabWidget::addTab(widget,icon,label);
}

int QTabWidgetExt::addTab(QWidget *widget, const QString &label, QWidget *w, QIcon wi, QString wt)
{   
    int r = QTabWidget::addTab(widget,label);
    insertButton(count()-1,w,wi,wt);
    return r;
}

int QTabWidgetExt::addTab(QWidget *widget, const QIcon& icon, const QString &label, QWidget *w, QIcon wi, QString wt)
{
    int r = QTabWidget::addTab(widget,icon,label);
    insertButton(count()-1,w,wi,wt);
    return r;
}

int QTabWidgetExt::insertTab(int index, QWidget *widget, const QString &label)
{
    return QTabWidget::insertTab(index,widget,label);
}

int QTabWidgetExt::insertTab(int index, QWidget *widget, const QIcon& icon, const QString &label)
{
    return QTabWidget::insertTab(index,widget,icon,label);
}

int QTabWidgetExt::insertTab(int index, QWidget *widget, const QString &label, QWidget *w, QIcon wi, QString wt)
{
    int r = QTabWidget::insertTab(index,widget,label);
    insertButton(index,w,wi,wt);
    return r;
}

int QTabWidgetExt::insertTab(int index, QWidget *widget, const QIcon& icon, const QString &label, QWidget *w, QIcon wi, QString wt)
{
    int r = QTabWidget::insertTab(index,widget,icon,label);
    insertButton(index,w,wi,wt);
    return r;
}

void QTabWidgetExt::insertButton(int index, QWidget *w, QIcon icon, QString text)
{
    QToolButton *b = new QToolButton(this);
    resultWin   *r = new resultWin;

    QColor  color = palette().color(QPalette::ColorRole::Highlight);
    QString style = "QToolButton{background-color: rgba(255, 255, 255, 0); border: 0px;} QToolButton:hover:!pressed{background-color: rgb(%1,%2,%3) ;}";

    b->setToolTip("Open in new window");
    b->setIcon(QIcon(":icons/window-new.svg")); 

    b->setStyleSheet(style.arg(color.red()).arg(color.green()).arg(color.blue()));
    b->setMinimumWidth(16);
    b->setMinimumHeight(16);
    b->setProperty(PRO_TAB.toStdString().c_str(),qVariantFromValue(w));
    b->setProperty(PRO_WIN.toStdString().c_str(),qVariantFromValue(r));
    b->setProperty(PRO_ICON.toStdString().c_str(),icon);
    b->setProperty(PRO_TEXT.toStdString().c_str(),text);
    w->setProperty(PRO_WIN.toStdString().c_str(),qVariantFromValue(r));
    connect(b, SIGNAL( clicked() ), this, SLOT( openTriggered() ) );

    rw.insert(index,r);
    QTabWidget::tabBar()->setTabButton(index, QTabBar::LeftSide, b);
}

void QTabWidgetExt::hideWindows()
{
    for(int i=0;i<rw.count();i++)
        rw[i]->hide();
}
