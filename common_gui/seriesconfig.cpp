#include "seriesconfig.h"
#include "ui_seriesconfig.h"
#include "commonapp.h"

#include <QPainter>

seriesConfig::seriesConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::seriesConfig)
{
    ui->setupUi(this);
    setBackgroundColor(ui->cbLine,Qt::white);
}

seriesConfig::~seriesConfig()
{
    delete ui;
}

void seriesConfig::deleteStyleSheet()
{
    ui->lTitle->setStyleSheet(sEmpty);
}

void seriesConfig::setLineTextColor()
{
    setBackgroundColor(ui->leColor,pen.color());
}

void seriesConfig::setPointTextColor()
{
    setBackgroundColor(ui->leColorPoint,pointColor);
}

void seriesConfig::setFontText()
{
    ui->leFont->setText(fontToString(pointFont));
}

void seriesConfig::setPen(QPen p)
{
    pen = p;
    // Color
    setLineTextColor();
    // Width
    ui->sbWidth->setValue(pen.width());
    // Style
    setLineStyleCombo(ui->cbLine,pen);
}


void seriesConfig::setName(QString n)
{
    ui->leName->setText(n);
}

void seriesConfig::setPoints(bool b)
{
    ui->chkPoints->setChecked(b);
}

void seriesConfig::setPointsLabel(bool b)
{
    ui->chkPointsLabel->setChecked(b);
}

void seriesConfig::setPointsColor(QColor c)
{
    pointColor = c;
    setPointTextColor();
}

void seriesConfig::setPointsFormat(QString f)
{
    ui->leFormat->setText(f);
}

void seriesConfig::setPointsClippping(bool b)
{
    ui->chkClipping->setChecked(b);
}

QPen seriesConfig::getPen()
{
    // Color -- already set
    // Width -- already set
    // Style -- already set

    return pen;
}

QString seriesConfig::getName()
{
    return ui->leName->text();
}

bool seriesConfig::getPoints()
{
    return ui->chkPoints->checkState() == Qt::Checked;
}

bool seriesConfig::getPointsLabel()
{
    return ui->chkPointsLabel->checkState() == Qt::Checked;
}

bool seriesConfig::getPointsClipping()
{
    return ui->chkClipping->checkState() == Qt::Checked;
}


QColor seriesConfig::getPointsColor()
{
    return pointColor;
}

QString seriesConfig::getPointsFormat()
{
    return ui->leFormat->text();
}

void seriesConfig::setOpacity(double o)
{
    ui->sbOpacity->setValue((int)(o*100));
}

double seriesConfig::getOpacity()
{
    return ((double)ui->sbOpacity->value())/100;
}

void seriesConfig::on_tbColor_clicked()
{
    dColor.setCurrentColor(pen.color());
    dColor.setWindowIcon(windowIcon());
    if (dColor.exec())
    {
        pen.setColor(dColor.currentColor());
        setLineTextColor();
        setLineStyleCombo(ui->cbLine,pen);
    }
}

void seriesConfig::on_tbColorPoint_clicked()
{
    dColor.setCurrentColor(pointColor);
    dColor.setWindowIcon(windowIcon());
    if (dColor.exec())
    {
        pointColor = dColor.currentColor();
        setPointTextColor();
    }
}

void seriesConfig::setPointsFont(QFont f)
{
    pointFont = f;
    setFontText();
}

QFont seriesConfig::getPointsFont()
{
    return pointFont;
}

void seriesConfig::on_tbFont_clicked()
{
    dFont.setCurrentFont(pointFont);
    dFont.setWindowIcon(windowIcon());
    if (dFont.exec())
    {
        pointFont = dFont.currentFont();
        setFontText();
    }
}

void seriesConfig::on_cbLine_currentIndexChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
    pen.setStyle((Qt::PenStyle)ui->cbLine->currentIndex());
}

void seriesConfig::on_sbWidth_valueChanged(int arg1)
{
    Q_UNUSED(arg1);

    pen.setWidth(ui->sbWidth->value());
    setLineStyleCombo(ui->cbLine,pen);
}
