#ifndef BRUSHCONF_H
#define BRUSHCONF_H

#include <QDialog>
#include <QBrush>
#include <QColorDialog>

namespace Ui {
class brushConf;
}

class brushConf : public QDialog
{
    Q_OBJECT

public:
    explicit brushConf(QWidget *parent = 0);
    ~brushConf();
    void setBrush(QBrush b);
    QBrush getBrush();
    void checkEnable();
    void brushToInterface();
    void interfaceToBrush();
    void deleteSytyleSheet();

private slots:
    void on_tbColor_clicked();
    void on_rbColor_clicked();
    void on_rbGradient_clicked();
    void on_rbTexture_clicked();

    void on_cbStyle_currentIndexChanged(int index);

private:
    Ui::brushConf *ui;
    QBrush       brush;
    QColorDialog dColor;
};

#endif // BRUSHCONF_H
