#ifndef OUTPUTS_H
#define OUTPUTS_H

#include <QDialog>
#include <QKeyEvent>

#include "opt_project.h"

namespace Ui {
class outputs;
}

class outputs : public QDialog
{
    Q_OBJECT

public:
    explicit outputs(QWidget *parent = 0);
    ~outputs();
    void keyPressEvent(QKeyEvent *e);
    void setModelExp(opt_project prj);
    opt_exp getExp();
    void checkEnabled();
    void deleteStyleSheet();

    QString company;
    QString appName;

protected:
    opt_exp   exp;
    opt_model mo;
    QList<QWidget *> lw;

private slots:
    void on_lineFilter_2_returnPressed();
    void on_buttonBox_accepted();
    void on_rbIntervals_clicked();
    void on_rbTime_clicked();
    void on_tbClear_clicked();

private:
    Ui::outputs *ui;
};

#endif // OUTPUTS_H
