#ifndef COMMONAPP_H
#define COMMONAPP_H

#include <QtWidgets/QTreeWidget>
#include <QRegExp>
#include <QString>
#include <QMainWindow>
#include <QDialog>
#include <QHeaderView>
#include <QComboBox>
#include <QLineEdit>
#include <QLabel>
#include <QtWidgets/QTableWidget>
#include <QChartView>
#include <QtCharts/chartsnamespace.h>
//#include <QAbstract3DGraph>
#include <QSplitter>
#include <QLineSeries>
#include <QAreaSeries>
#include <QBarSeries>
#include <exception>

#include "opt_project.h"
#include "common.h"

#include "import/base/include/FMUModelExchange_v2.h"

//using namespace QtDataVisualization;

// Class names
const QString cQChartView  = "QtCharts::QChartView";

// Separator for settings
const QString pSep = ".";

// Properties for settings
const QString pGeometry = "geometry";
const QString pState    = "state";

// Double values
const double MAX_DOUBLE = 1e300;
const int    NUM_DEC    = 8;
const char   NUM_FORMAT = 'g';

// Some symbols
const QString space     = " ";
const QString comma     = ",";
const QString unitLeft  = "[";
const QString unitRight = "]";
const QString quote     = "\"";
const QString asterisk  = "*";
const QString script    = "-";

// Bool strings
const QString sTrue  = "True";
const QString sFalse = "False";

// WARNING: icons should be arguments
const QString iStruct        = ":/icons/struct-icon.svg";
const QString iInteger       = ":/icons/integer-icon.svg";
const QString iReal          = ":/icons/real-icon.svg";
const QString iBool          = ":/icons/bool-icon.svg";
const QString iString        = ":/icons/string-icon.svg";
const QString iClear         = ":/icons/edit-clear";
const QString iWindow        = ":/icons/window.svg";
const QString iParameters    = ":/icons/configure";
const QString iHorizontal    = ":/icons/transform-move-horizontal.svg";
const QString iVertical      = ":/icons/transform-move-vertical.svg";
const QString iLoadProject   = ":/icons/document-open";
const QString iListRemove    = ":/icons/list-remove.svg";
const QString iWindowClose   = ":/icons/window-close.svg";
const QString iZoom          = ":/icons/zoom.svg";
const QString iZoomOut       = ":/icons/zoom-out.svg";
const QString iTabNew        = ":/icons/tab-new.svg";
const QString iStyle         = ":/icons/format-border-style.svg";
const QString iUndefined     = ":/icons/undefined.svg";
const QString iParameter     = ":/icons/parameter.svg";
const QString iCalculated    = ":/icons/calculated.svg";
const QString iInput         = ":/icons/input.svg";
const QString iOutput        = ":/icons/output.svg";
const QString iLocal         = ":/icons/local.svg";
const QString iIndependent   = ":/icons/independent.svg";
const QString iConstant      = ":/icons/constant.svg";
const QString iFixed         = ":/icons/fixed.svg";
const QString iTunable       = ":/icons/tunable.svg";
const QString iDiscrete      = ":/icons/discrete.svg";
const QString iContinuous    = ":/icons/continuous.svg";
const QString iCSV           = ":/icons/csv.svg";
const QString iTRJ           = ":/icons/trj.svg";
const QString iData          = ":/icons/data.svg";
const QString iIntegrator    = ":/icons/labplot-xy-fourier-transform-curve";
const QString iClipboard     = ":icons/document-export.svg";
const QString iSave          = ":icons/document-save.svg";
const QString iGraphArea     = ":icons/office-chart-area-black.svg";
const QString iGraphLine     = ":icons/office-chart-line.svg";
const QString iGraphBar      = ":icons/office-chart-bar.svg";
const QString iGraphPie      = ":icons/office-chart-pie.svg";
const QString iGraph         = ":icons/labplot-xy-equation-curve.svg";
const QString iGraphWhite    = ":icons/labplot-xy-equation-curve-white.svg";

// WARNING: they should be arguments
const int PAR_COL_NAME    = 0;
const int PAR_COL_VALUE   = 1;
const int PAR_COL_DESC    = 2;
const int PAR_COL_UNIT    = 3;
const int PAR_COL_MIN     = 4;
const int PAR_COL_MAX     = 5;

// WARNING: they should be arguments
const QString PAR_PRO_VALUE = "PRO_VALUE";
const QString PAR_PRO_POS   = "PRO_POS";

// WARNING: they should be arguments
const QString MIN_T          = "MIN_T";
const QString MAX_T          = "MAX_T";
const QString MIN_Y          = "MIN_Y";
const QString MAX_Y          = "MAX_Y";
const QString VAR_INDEX      = "VAR_INDEX";
const QString VAR_NAME       = "VAR_NAME";
const QString VAR_TIME_VAL   = "VAR_TIME_VAL";
const QString VAR_COUNT      = "VAR_COUNT";
const QString VAR_CHARTVIEW  = "VAR_CHARTVIEW";
const QString VAR_EXPRESSION = "VAR_EXPRESSION";
const QString VAR_VARS       = "VAR_VARS";
const QString VAR            = "VAR";
const double RANGE_T         = 1e-4;
const double RANGE_Y         = 1e-4;
const double MIN_RANGE       = 1e-200;

// Series hint
const QString REF_SER = "REF_SER";

const QString mImagFile  = "Save to image file";
const QString mTextFile  = "Save to text file";
const QString mVideoFile = "Save to video file";

// Extensions
const QString ePDF  = "pdf";
const QString eSVG  = "svg";
const QString ePNG  = "png";
const QString eBMP  = "bmp";
const QString eJPG  = "jpg";
const QString eJPEG = "jpeg";
const QString eAVI  = "avi";

// Filters
const QString PDFFilter  = "PDF file (*."+ePDF+") (*."+ePDF+")";
const QString SVGFilter  = "SVG file (*."+eSVG+") (*."+eSVG+")";
const QString PNGFilter  = "PNG file (*."+ePNG+") (*."+ePNG+")";
const QString BMPFilter  = "BMP file (*."+eBMP+") (*."+eBMP+")";
const QString JPGFilter  = "JPG file (*."+eJPG+") (*."+eJPG+")";
const QString JPEGFilter = "JPEG file (*."+eJPEG+") (*."+eJPEG+")";
const QString CSVFilter  = "CSV file (*."+eCSV+")";
const QString AVIFilter  = "AVI file (*."+eAVI+")";

// New line and tab characters
const QString newLine = "\n";
const QString tab     = "\t";

#ifdef __linux__
    const QString defTextEditor = "kwrite";
#elif defined(__MINGW32__)
    const QString defTextEditor = "notepad";
#else
    const QString defTextEditor = "notepad";
#endif
const bool     defToolBar    = false;
const QString  optTextEditor = "textEditor";
const QString  optToolBar    = "toolBar";
const unsigned NUM_BYTES_LOG = 10000;
void openFileInEditor(QString file, QString editor);

void chartToImageFile(QWidget *parent, QChartView *qcv);
void imageToFile(QWidget *parent, QImage image);

void chartToPDF(QGraphicsView *qcv, QString file);
void chartToSVG(QGraphicsView *qcv, QString file);
void chartToImage(QGraphicsView *qcv, bool clipboard = true, QString file = sEmpty);

//void chartToPDF(QAbstract3DGraph *qcv, QString file);
//void chartToSVG(QAbstract3DGraph *qcv, QString file);
//void chartToImage(QAbstract3DGraph *qcv, bool clipboard = true, QString file = sEmpty);

void imageToPDF(QImage image, QString file);
void imageToSVG(QImage image, QString file);
void imageToImg(QImage image, bool clipboard, QString file);

void serieToData(QLineSeries *serie, QList<double> &time, QList<double> &values, QString &name);
void serieToDataForTimeInt(QList<QPointF> points, QList<double> time, QList<double> &values);
void chartToData(QChartView *qcv, QList<double> &time, QList<QList<double>> &vars, QList<QString> &names);
void copyVarsClipboard(QList<double> time, QList<QList<double>> vars, QList<QString> names);
void copyVarsFile(QWidget *parent, QList<double> time, QList<QList<double>> vars, QList<QString> names);

QString axisName(QList<QAbstractAxis *> axis, Qt::Alignment a);

typedef void (*setItemTreePointer)(QTreeWidget *, QTreeWidgetItem *, opt_project, variable, QString, int, QList<QWidget *> &);

void    filterTree(QTreeWidget *tree, int textPos, QString cad, bool caseSensitive);
QString integratorToText(IntegratorType it);
void    populateTreeGeneric(QTreeWidget *tree, QList<variable> vars, opt_project prj, int COL_NAME, QString var_selected, setItemTreePointer sit, QList<QWidget*> &lw, QTreeWidgetItem *topItem=NULL);

void setItemTreeParams(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable v, QString vs, int k, QList<QWidget *> &lw);

void writeGeometry(QString company, QString appName, QMainWindow *qw, QString id);
void readGeometry(QString company, QString appName, QMainWindow *qw, QString id);
void writeGeometry(QString company, QString appName, QDialog *qw, QString id);
void readGeometry(QString company, QString appName, QDialog *qw, QString id);
void writeState(QString company, QString appName, QHeaderView *qw, QString id);
void readState(QString company, QString appName, QHeaderView *qw, QString id);
void writeState(QString company, QString appName, QSplitter *s, QString id);
void readState(QString company, QString appName, QSplitter *s, QString id);
void writeGeometry(QString company, QString appName, QTableWidget *table, QString id);
void readGeometry(QString company, QString appName, QTableWidget *table, QString id);
void writeState(QString company, QString appName, QTableWidget *table, QString id);
void readState(QString company, QString appName, QTableWidget *table, QString id);
void writeOption(QString company, QString appName, QString option, QString value);
void writeOption(QString company, QString appName, QString option, bool value);
void writeOption(QString company, QString appName, QString option, int value);
QString readOption(QString company, QString appName, QString option, QString defValue);
bool readOption(QString company, QString appName, QString option, bool defValue);
int readOption(QString company, QString appName, QString option, int defValue);

unsigned checkInputFile(QString file, unsigned &format, QList<variable> &vars, QList<double> &first, QList<double> &last, int &num_mea, QString &error);
unsigned readVarsMATfile(QString file, QList<variable> &vars, QList<double> &first, QList<double> &last,  int &num_mea, QString &error);
unsigned readVarsCSVfile(QString file, QList<variable> &vars, QList<double> &first, QList<double> &last, int &num_mea, QString &error);
template <typename T>
unsigned getDataFromFile(QString file, unsigned format, QStringList names, QList<QList<T>> &values, QString &error);

QString formatedUnit(opt_model m, QList<variable> v, int k);

void setModelParameters(fmi_2_0::FMUModelExchange &fmu, opt_model mo);
void setModelInputs(fmi_2_0::FMUModelExchange &fmu, opt_exp exp, fmi2Real time);

void     setBackgroundColor(QLineEdit *w, QColor c);
void     setBackgroundColor(QComboBox *w, QColor c);
void     setLineStyleCombo(QComboBox *cb, QPen pen);
void     setBrushStyleCombo(QComboBox *cb, QBrush brush);
void     setLineStyleLabel(QLabel *l, QPen pen);
void     setBrushStyleLabel(QLabel *l, QBrush brush);
QString  fontToString(QFont f);
int      lineWidth(QString s, QFont f);
int      lineHeight(QFont f);
variable varTimeDef();

void setValidator(QLineEdit *le, double min, double max);

void setAxesRangeLine(QChartView *qcv, Qt::AlignmentFlag y_align = Qt::AlignLeft, bool isEmpty = false);
void rangeSameValues(double &min, double &max, double RANGE);
void rangeSameValues(double &min, double &max, double RANGE);

bool focusItemGraph(QWidget *&w);
void setEmptyAxes(QtCharts::QChartView *qcv);

void serieHover(QPointF point, bool state, QObject *sender, QMenu *cmSerie, int index=-1);
void removeSerieHint();
void sliceHover(bool state, QObject *sender);
void barSetHover(bool state,int index, QObject *sender);
bool selectedSerie();

class ex_parEst_stop: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Parameter estimation stopped";
  }
};

class QChartViewExt: public QChartView
{
    public:
        void setObjNull();

    private:
        QObject *obj = NULL;

    protected:
        void mouseReleaseEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
};

QString toLocalDecimalSeparator(QString s);
QString copy(QTableWidget *table, int top, int left, int rowCount, int colCount, bool header = false, QString colSeparator = tab, QString rowSeparator = newLine);
void copyClipboard(QTableWidget *table, int top, int left, int rowCount, int colCount, bool header = false);

#endif // COMMONAPP_H
