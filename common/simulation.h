#ifndef SIMULATION_H
#define SIMULATION_H

#include <QtCore>

#include "opt_project.h"
#include "common.h"
#include "import/base/include/FMUModelExchange_v2.h"

class simulation: public QObject
{
    Q_OBJECT

private:
    opt_project  *prj;
    int          t_ms;
    bool         stop;
    fmiTime      time;
    fmiStatus    status;

    void setModelOutputs(fmi_2_0::FMUModelExchange *fmu, opt_model mo, opt_exp &exp, fmi2Real time);

public:
    simulation();
    simulation(opt_project *p);
    ~simulation();
    int          getSimTime();
    void         stopSim();
    opt_project *getProject();
    void         setProject(opt_project *p);
    unsigned     getProjectType();
    void         error(unsigned ERROR_CODE, opt_exp e);
    fmiStatus    getStatus() const;
    QString      getLogText() const;

public slots:
    void run(bool unpackFMU = true, bool buildData = true);

signals:
    void finished(double, opt_exp, opt_problem, unsigned);
    void end();
    void simUpdate(double t, double tstart, double tstop, double elapsed, opt_exp exp);
    void simError(unsigned s, QString msg);
};

QString getLogText();
void clearLogText();
void infoToLog();


#endif // SIMULATION_H
