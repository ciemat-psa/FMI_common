#include "opt_project.h"
#include <JlCompress.h>

//#include "confapp.h"
#include "common.h"
#include "common_template.h"

#include "common/FMIPPConfig.h"
#include "import/base/include/ModelManager.h"
#include "import/base/include/ModelDescription.h"

QString opt_project::getFilename() const
{
    return filename;
}

void opt_project::setFilename(const QString &value)
{
    filename = value;
}

int opt_project::getType() const
{
    return type;
}

void opt_project::setType(int value)
{
    type = value;
}

void opt_project::setChanged(bool value)
{
    changed = value;
}

opt_problem opt_project::getPrb() const
{
    return prb;
}

void opt_project::setPrb(const opt_problem &value)
{
    prb = value;
    setChanged(true);
}

double opt_project::getPercentage() const
{
    return percentage;
}

void opt_project::setPercentage(double value)
{
    percentage = value;
}

QStringList opt_project::getLmesg() const
{
    return lmesg;
}

void opt_project::setLmesg(const QStringList &value)
{
    lmesg = value;
}

QList<unsigned> opt_project::getLtype() const
{
    return ltype;
}

void opt_project::setLtype(const QList<unsigned> &value)
{
    ltype = value;
}

QStringList opt_project::getLtime() const
{
    return ltime;
}

void opt_project::setLtime(const QStringList &value)
{
    ltime = value;
}

double opt_project::getPerTime() const
{
    return perTime;
}

void opt_project::setPerTime(double value)
{
    perTime = value;
}

opt_project::opt_project(int type)
{
    this->type = type;
    changed    = false;
    filename   = sEmpty;
}

void opt_project::deletePrbMethodArgs()
{
    // WARNING
    // -------
    // Since dynamic-memory arguments in method from problem are pointers
    // and project, problem and method are copied, arguments are free here
    // this must be modified.

    deleteArgs(getPrb().getMethod().getArgs());
}

opt_project::~opt_project()
{

}

bool opt_project::load_from_file(QString filename)
{
    deletePrbMethodArgs();
    setFilename(filename);

    QFile file(getFilename());
    if (!file.exists()) return false;
    QDataStream in(&file);

    file.open(QIODevice::ReadOnly);
    in >> *this;
    file.close();
    changed = false;
    return true;
}

void opt_project::save_to_file()
{
    QFile file(filename);
    QDataStream out(&file);

    file.open(QIODevice::WriteOnly);
    out << *this;
    file.close();
    changed = false;
}

bool opt_project::getChanged() const
{
    return changed;
}

opt_model::opt_model()
{
    valid    = FMU_NO_FILE;
    expData  = false;
    expStart = 0;
    expStop  = 1;
}

int opt_model::findParam(QString p)
{
    return findVar(p,params);
}

int opt_model::findInput(QString i)
{
    return findVar(i,inputs);
}


opt_model opt_project::getModel() const
{
    return model;
}

void opt_project::setModel(const opt_model &value)
{
    model = value;
    changed = true;
}

QString opt_project::getName() const
{
    return name;
}

void opt_project::setName(const QString &value)
{
    name = value;
    changed = true;
}

opt_sim opt_project::getSim() const
{
    return sim;
}

void opt_project::setSim(const opt_sim &value)
{
    sim = value;
    changed = true;
}

opt_exp opt_project::getExp() const
{
    return exp;
}

void listType(QList<variable> list, unsigned type, QStringList &newList)
{
    newList.clear();
    for(int i=0;i<list.count();i++)
    {
        if (list[i].type == type)
            newList.append(list[i].name);
    }
}

template <typename T>
QList<QMap<double,T> > inputType(QStringList iName, int iniTimePos, opt_project *prj, opt_exp *exp, QList<QList<T> > inp_traj)
{
    int              pos_traj = 0;
    QList<QList<T> > values_type;

    for(int i=0;i<iName.count();i++)
        values_type.append(QList<T>());

    #pragma omp parallel for shared(exp,values_type)
    for(int i=0;i<iName.count();i++)
    {
        int         pos;
        unsigned    type_input;
        double      fixed;
        QString     filename;
        unsigned    fileformat;
        QString     fileinputs;
        QList<T>    vals;

        #pragma omp critical
        {
            pos        = exp->model_inputs.indexOf(iName[i]);
            type_input = pos>=0 ? exp->type_inputs[pos] : 0;
            fixed      = pos>=0 ? exp->fixed_inputs[pos].toDouble() : 0;
            filename   = exp->fileName;
            fileformat = exp->fileFormat;
            fileinputs = pos>=0 ? exp->file_inputs[pos] : sEmpty;
        }

        // If found in experiment
        if (pos>=0)
        {
            switch (type_input)
            {
                case ttFixed:
                {
                    vals.append(fixed);
                    break;
                }
                case ttFile:
                {
                    QStringList      field_file;
                    QList<QList<T> > values_file;
                    QString          error;

                    field_file.append(fileinputs);
                    unsigned res = getDataFromFile(fileAbsoluteFromProject(prj,filename),fileformat,field_file,values_file,error);
                    if (res == FILE_OK) vals.append(values_file[0]);
                    break;
                }
                case ttList:
                {
                    vals.append(inp_traj[pos_traj]);
                    pos_traj++;
                    break;
                }
                case ttFree:
                {
                    // Fixed to zero
                    vals.append(0);
                    break;
                }
            }
        }
        // If not found, then set to zero
        else
        {
            vals.append(0);
        }
        values_type.replace(i,vals);
    }

    QList<QMap<double,T> > input_type_values;

    if (iName.count()>0)
    {
        input_type_values.clear();
        for(int i=0;i<values_type.count();i++)
        {
            // Type of input: ttFree, ttFixed, ttFile or ttList.
            int pos = exp->model_inputs.indexOf(iName[i]);
            // ttList input begins from 0
            int k = pos>=0 && exp->type_inputs[pos] == ttList ? 0 : iniTimePos;
            QMap<double,T> mr;

            for(unsigned j=0;j<exp->timeIns.count();j++)
            {
                if (exp->timeIns.getType(j) == TT_INPUT || exp->timeIns.getType(j) == TT_IN_OUT)
                {
                    double value = values_type[i].count() <= 0 ? 0 :
                               values_type[i].count() >  k ? values_type[i][k] : values_type[i].last();
                    mr[exp->timeIns.getValue(j)] = value;
                    // ttList input has the exact number of inputs
                    if (pos>=0 && exp->type_inputs[pos] == ttList)
                        k++;
                    else
                        k = (exp->timeInType == ttTimeFile) ? k+exp->ReducSam : k+1;
                }
            }
            input_type_values.push_back(mr);
        }
    }
    return input_type_values;
}


unsigned opt_exp::buildData(QList<variable> inputs, opt_project *prj, QList<QList<double> > inp_traj)
{
    // Intial time position in file / 0 for interval
    int iniTimePos = -1;

    // Input names by FMI type
    listType(inputs,FMI_REAL,iRealName);
    listType(inputs,FMI_INTEGER,iIntName);
    listType(inputs,FMI_BOOL,iBoolName);
    listType(inputs,FMI_STRING,iStringName);

    // Clear time instants and output times and values
    timeIns.clear();
    time_output.clear();
    values_output.clear();
    values_input.clear();

    // ----------------- //
    // Build time vector //
    // ----------------- //

    // --------------- //
    // 1.- Input times //
    // --------------- //

    if (iRealName.count()>0) // || iIntName.count()>0 || iBoolName.count()>0 || iStringName.count()>0)
    {
        // 1.a.- Input times - linear distribution (sorted)
        if (timeInType == ttTimeDist)
        {
            double step = (stopTime-startTime)/nInInt;
            double time = startTime;

            iniTimePos = 0;
            for (int i=0;i<=nInInt;i++)
            {
                timeIns.insert(time,TT_INPUT);
                time += step;
            }
        }
        // 1.b - Input times - from file (sorted)
        else if (timeInType == ttTimeFile)
        {
            QString               error;
            QStringList           names;
            QList<QList<double> > values;
            unsigned              res;
            int                   i = 0;
            int                   posIni=-1;
            unsigned              TT_TYPE;

            names.append(timeVar);

            res = getDataFromFile(fileAbsoluteFromProject(prj,fileName),fileFormat,names,values,error);
            if (res != FILE_OK) return res;

            // Find initial position
            while(i<values[0].count() && values[0][i]<startTime) i++;
            iniTimePos = i;

            // Add point for having a value in case of interpolation
            // at stop time (if there is at least one value)
            if (iniTimePos-1>=0 && i-1<values[0].count())
            {
                posIni=i-1;
                iniTimePos--;
            }

            // Input times are also output times in input estimation
            TT_TYPE = prj->getType() == ptInputEst ? TT_IN_OUT : TT_INPUT;

            // Time input instants
            while(i<values[0].count() && values[0][i]<=stopTime)
            {
                timeIns.insert(values[0][i],TT_TYPE);
                i=i+ReducSam;
            }

            // Add extra point for having a value in case of interpolation
            // at stop time (if there is at least one value)
            if (timeIns.count()>0)
            {
                if (posIni!=-1) timeIns.insert(values[0][posIni],TT_TYPE);
                while (i>=values[0].count()) i--;
                timeIns.insert(values[0][i],TT_TYPE);
            }
        }
        else
            return SIM_FMU_TIME_TYPE_INVALID;
    }

    // ---------------- //
    // 2.- Output times //
    // ---------------- //

    // 2.a.- Output times - linear distribution (sorted)
    // 2.b.- Output times - step size in seconds (sorted)
    if (timeOutType == ttTimeDist || timeOutType == ttTimeStep)
    {
        double time = startTime;
        double step = timeOutType == ttTimeDist ? (stopTime-startTime)/nOutInt : stepOutTime;
        int    index;

        while(time<=stopTime)
        {
            index = timeIns.indexOf(time);
            if (index == -1) timeIns.insert(time,TT_OUTPUT);
            else             timeIns.setType(index,TT_IN_OUT);
            time += step;
        }

        // Output point at the simulation end
        index = timeIns.indexOf(stopTime);
        if (index == -1) timeIns.insert(stopTime,TT_OUTPUT);
        else if (timeIns.getType(index) == TT_INPUT) timeIns.setType(index,TT_IN_OUT);
    }
    else
        return SIM_FMU_TIME_TYPE_INVALID;

    // -------------------- //
    // 3.- Set input values //
    // -------------------- //

//    QList<QList<double> > values_real;

//    // 3.a.- Real inputs
//    // -----------------
//    int pos_traj = 0;

//    // Kind of input: free, fixed or file
//    for(int i=0;i<iRealName.count();i++)
//    {
//        int pos = model_inputs.indexOf(iRealName[i]);
//        QList<double> vals;

//        // If found in experiment
//        if (pos>=0)
//        {
//            switch (type_inputs[pos])
//            {
//                case ttFixed:
//                {
//                    vals.append(fixed_inputs[pos].toDouble());
//                    break;
//                }
//                case ttFile:
//                {
//                    QStringList           field_file;
//                    QList<QList<double> > values_file;
//                    QString               error;

//                    field_file.append(file_inputs[pos]);
//                    unsigned res = getDataFromFile(fileAbsoluteFromProject(prj,fileName),fileFormat,field_file,values_file,error);
//                    if (res == FILE_OK) vals.append(values_file[0]);
//                    break;
//                }
//                case ttList:
//                {
//                    vals.append(inp_traj[pos_traj]);
//                    pos_traj++;
//                    break;
//                }
//                case ttFree:
//                {
//                    // Fixed to zero
//                    vals.append(0);
//                    break;
//                }
//            }
//        }
//        // If not found, then set to zero
//        else
//        {
//            vals.append(0);
//        }
//        values_real.append(vals);
//    }

//    if (iRealName.count()>0)
//    {
//        input_real_values.clear();
//        for(int i=0;i<values_real.count();i++)
//        {
//            // Type of input: ttFree, ttFixed, ttFile or ttList.
//            int pos = model_inputs.indexOf(iRealName[i]);
//            // ttList input begins from 0
//            int k   = pos>=0 && type_inputs[pos] == ttList ? 0 : iniTimePos;
//            QMap<double,double> mr;

//            for(unsigned j=0;j<timeIns.count();j++)
//            {
//                if (timeIns.getType(j) == TT_INPUT || timeIns.getType(j) == TT_IN_OUT)
//                {
//                    double value = values_real[i].count() <= 0 ? 0 :
//                                   values_real[i].count() >  k ? values_real[i][k] : values_real[i].last();
//                    mr[timeIns.getValue(j)] = value;
//                    // ttList input has the exact number of inputs
//                    if (pos>=0 && type_inputs[pos] == ttList)
//                        k++;
//                    else
//                        k = (timeInType == ttTimeFile) ? k+ReducSam : k+1;
//                }
//            }
//            input_real_values.push_back(mr);
//        }
//    }

    // 3.a - Real inputs
    input_real_values = inputType(iRealName,iniTimePos,prj,this,inp_traj);

    // 3.b.- Integer inputs
    QList<QList<int> > inp_traj_int; // WARNING: see why this should be exported!!
    input_int_values = inputType(iIntName,iniTimePos,prj,this,inp_traj_int);

    // 3.c.- Boolean inputs - ToDo
    input_bool_values.clear();

    // 3.d.- String inputs - ToDo
    input_string_values.clear();

    return SIM_FMU_VALID;
}

std::string *opt_exp::getStringList(QStringList qList)
{
    if (qList.count()>0)
    {
        std::string *arr = new std::string[qList.count()];

        for(int i=0;i<qList.size();i++)
            arr[i] = qList[i].toStdString();
        return arr;
    }
    return NULL;
}

std::string *opt_exp::getiRealNames(){return getStringList(iRealName);}
std::string *opt_exp::getiIntNames(){return getStringList(iIntName);}
std::string *opt_exp::getiBoolNames(){return getStringList(iBoolName);}
std::string *opt_exp::getiStringNames(){return getStringList(iStringName);}

//std::string *opt_exp::getoRealNames(){return getStringList(oRealName);}
//std::string *opt_exp::getoIntNames(){return getStringList(oIntName);}
//std::string *opt_exp::getoBoolNames(){return getStringList(oBoolName);}
//std::string *opt_exp::getoStringNames(){return getStringList(oStringName);}

unsigned opt_exp::getiRealCount(){return iRealName.count();}
unsigned opt_exp::getiIntCount(){return iIntName.count();}
unsigned opt_exp::getiBoolCount(){return iBoolName.count();}
unsigned opt_exp::getiStringCount(){return iStringName.count();}

//unsigned opt_exp::getoRealCount(){return oRealName.count();}
//unsigned opt_exp::getoIntCount(){return oIntName.count();}
//unsigned opt_exp::getoBoolCount(){return oBoolName.count();}
//unsigned opt_exp::getoStringCount(){return oStringName.count();}

void time_sim::insert (double v, unsigned t)
{
    int  index = 0;
    bool found = false;

    while(index<value.count() && !found)
    {
        found = value[index]>=v;
        if (!found) index++;
    }

    if (found)
    {
        value.insert(index,v);
        type.insert(index,t);
    }
    else
    {
        value.push_back(v);
        type.push_back(t);
    }
}

void time_sim::clear()
{
    value.clear();
    type.clear();
}

unsigned time_sim::count()
{
    return value.count();
}

double time_sim::getValue(unsigned i)
{
    return value[i];
}

unsigned time_sim::getType(unsigned i)
{
    return type[i];
}

int time_sim::indexOf(double t)
{
    return value.indexOf(t);
}

double linearInterpolation(double xi, double x1, double y1, double x2, double y2)
{
    return y1 + (xi-x1)*((y2-y1)/(x2-x1));
}

unsigned time_sim::lowerBound(double v)
{
    QList<double>::const_iterator it = std::lower_bound(value.constBegin(),value.constEnd(),v);

    return it - value.constBegin();
}

QList<double>   time_sim::getValue(){ return value;}
QList<unsigned> time_sim::getType(){ return type;}

void time_sim::setType(int i, unsigned t)
{
    type[i] = t;
}

double *opt_exp::getiRealVals(double time)
{
    int index = timeIns.indexOf(time);
    double *arr = new double[getiRealCount()];

    // Exact values for that time instant
    if (index>=0 && (timeIns.getType(index) == TT_INPUT || timeIns.getType(index) == TT_IN_OUT))
    {       
        // WARNING: problem reading with OpenMP from input_real_values
        // #pragma omp parallel for
        for(unsigned i=0;i<getiRealCount();i++)
            arr[i] = input_real_values[i][time];
    }
    // Interpolation between two data points
    else
    {
        // Locate interval
        unsigned pos = timeIns.lowerBound(time);

        if (pos>0 && pos<timeIns.count())
        {
            // Linear interpolation
            int      pos_x1 = pos-1;
            unsigned pos_x2 = pos;

            while (pos_x1>=0 && timeIns.getType(pos_x1)!=TT_INPUT && timeIns.getType(pos_x1)!=TT_IN_OUT)
                pos_x1--;

            while (pos_x2<timeIns.count() && timeIns.getType(pos_x2)!=TT_INPUT && timeIns.getType(pos_x2)!=TT_IN_OUT)
                pos_x2++;

            if (pos_x1>=0 && pos_x2<timeIns.count())
            {
                double x1 = timeIns.getValue(pos_x1);
                double x2 = timeIns.getValue(pos_x2);

                // WARNING: problem reading with OpenMP from input_real_values
                // #pragma omp parallel for
                for(unsigned i=0;i<getiRealCount();i++)
                {
                    double y1 = input_real_values[i][x1];
                    double y2 = input_real_values[i][x2];

                    arr[i] = linearInterpolation(time,x1,y1,x2,y2);
                }
            }
            else
            {
                //#pragma omp parallel for
                for(unsigned i=0;i<getiRealCount();i++)
                    arr[i] = 0;
            }
        }
        else
        {
            //#pragma omp parallel for
            for(unsigned i=0;i<getiRealCount();i++)
                arr[i] = 0;
        }
    }

    return arr;
}

int *opt_exp::getiIntVals(double time)
{
    int index = timeIns.indexOf(time);
    int *arr  = new int[getiIntCount()];

    // Exact values for that time instant
    if (index>=0 && (timeIns.getType(index) == TT_INPUT || timeIns.getType(index) == TT_IN_OUT))
    {
        // WARNING: problem reading with OpenMP from input_real_values
        // #pragma omp parallel for
        for(unsigned i=0;i<getiIntCount();i++)
        {
            arr[i] = input_int_values[i][time];
            //qDebug() << arr[i];
        }
    }
    // Previous data point
    else
    {
        // Locate interval
        unsigned pos = timeIns.lowerBound(time);

        for(unsigned i=0;i<getiIntCount();i++)
        {
            if (pos>0 && pos<timeIns.count())
                arr[i] = input_int_values[i][timeIns.getValue(pos-1)];
            else
                arr[i] = 0;
            //qDebug() << arr[i];
        }
    }
    return arr;
}

int *opt_exp::getiBoolVals(double time)
{
    Q_UNUSED(time);
    return NULL;
}

const char **opt_exp::getiStringVals(double time)
{
    Q_UNUSED(time);
    return NULL;
}

void opt_project::setExp(const opt_exp &value)
{
    exp     = value;
    changed = true;
}

void opt_project::setSimStatus(unsigned status)
{
    exp.simStatus = status;
}

opt_sim sim_restoreFromDefaults()
{
    // WARNING: company and appName needed in this function for
    // reading integrator default values.

//    // Restore from default
//    QSettings settings(company,appName);
    opt_sim   sim(false);

//    sim.order      = settings.value(stOrder,DEFAULT_ORDER).toDouble();
//    sim.stepSize   = settings.value(stStepSize,DEFAULT_STEP_SIZE).toDouble();
//    sim.absTol     = settings.value(stAbsTol,DEFAULT_ABS_TOL).toDouble();
//    sim.relTol     = settings.value(stRelTol,DEFAULT_REL_TOL).toDouble();
//    sim.integrator = (IntegratorType)settings.value(stInt,DEFAULT_INTEGRATOR).toUInt();

    sim.order      = DEFAULT_ORDER.toDouble();
    sim.stepSize   = DEFAULT_STEP_SIZE.toDouble();
    sim.absTol     = DEFAULT_ABS_TOL.toDouble();
    sim.relTol     = DEFAULT_REL_TOL.toDouble();
    sim.integrator = DEFAULT_INTEGRATOR;

    return sim;
}

opt_sim::opt_sim(bool restoreDefault)
{    
    Q_UNUSED(restoreDefault);

    // WARNING: company and appName needed in this class for
    // reading integrator default values.

    if (restoreDefault)
    {
        opt_sim s = sim_restoreFromDefaults();

        order      = s.order;
        stepSize   = s.stepSize;
        absTol     = s.absTol;
        relTol     = s.relTol;
        integrator = s.integrator;
    }
}
opt_exp::opt_exp()
{
    startTime   = DEFAULT_START_TIME;
    stopTime    = DEFAULT_STOP_TIME;
    simStatus   = SIM_FMU_NONE;
    nInInt      = DEFAULT_N_INTERVAL;
    timeInType  = DEFAULT_TYPE_TIME;
    ReducSam    = DEFAULT_REDUC_SAM;
    timeOutType = DEFAULT_OUT_TIME;
    nOutInt     = DEFAULT_OUT_INTERVAL;
    stepOutTime = DEFAULT_STEP_OUT;
}

QDataStream &operator<<(QDataStream &out, const opt_project &p)
{
    out << p.getType() << p.getName() << p.getModel() << p.getSim() << p.getExp() << p.getPrb()
        << p.getPercentage() << p.getLmesg() << p.getLtype() << p.getLtime() << p.getPerTime();
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_project &p)
{
    QString         name;
    opt_model       m;
    opt_sim         s;
    opt_exp         e;
    qint32          type;
    opt_problem     prb;
    double          per;
    QStringList     lme;
    QList<unsigned> lty;
    QStringList     lti;
    double          pTi;

    deleteArgs(prb.getMethod().getArgs());
    in >> type >> name >> m >> s >> e >> prb >> per >> lme >> lty >> lti >> pTi;

    p.setType(type);
    p.setName(name);
    p.setModel(m);
    p.setSim(s);
    p.setExp(e);
    p.setPrb(prb);
    p.setPercentage(per);
    p.setLmesg(lme);
    p.setLtype(lty);
    p.setLtime(lti);
    p.setPerTime(pTi);
    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_model &m)
{
    out << m.getFMUfileName() << m.p_name_new << m.p_value_new;
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_model &m)
{
    QString FMUfilename;

    in >> FMUfilename >> m.p_name_new >> m.p_value_new;

    m.setFMUfileName(FMUfilename,sEmpty);
    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_sim &s)
{
    out << (qint32)s.integrator << s.order << s.stepSize << s.absTol << s.relTol;
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_sim &s)
{
    qint32 buffer;

    in >> buffer >> s.order >> s.stepSize >> s.absTol >> s.relTol;

    s.integrator = IntegratorType(buffer);
    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_exp &e)
{
   out << e.startTime   << e.stopTime     << e.nInInt       << e.timeInType  << e.timeVar << e.fileName << e.fileFormat
       << e.file_inputs << e.fixed_inputs << e.model_inputs << e.type_inputs << e.fmiType_inputs << e.ReducSam
       << e.timeOutType << e.nOutInt      << e.stepOutTime  << e.time_output << e.values_output  << e.values_input;
   return out;
}


QDataStream &operator>>(QDataStream &in, opt_exp &e)
{
    in >> e.startTime   >> e.stopTime     >> e.nInInt       >> e.timeInType  >> e.timeVar >> e.fileName >> e.fileFormat
       >> e.file_inputs >> e.fixed_inputs >> e.model_inputs >> e.type_inputs >> e.fmiType_inputs >> e.ReducSam
       >> e.timeOutType >> e.nOutInt      >> e.stepOutTime  >> e.time_output >> e.values_output  >> e.values_input;
    return in;
}


QString opt_model::getFMUfileName() const
{
    return FMUfileName;
}

void opt_model::setFMUfileName(const QString &value, QString path)
{
    FMUfileName = value;
    if (!path.isEmpty())
        getModelInfo(path);
}

//QString opt_model::getWork_dir() const
//{
//    return work_dir;
//}


// TODO: FMI++ library does not povide information about
// support of ModelExchange and CoSimulation, so this is
// just reading from file. This must be changed.
void opt_model::checkModelExchangeCosimulation(QString filename)
{
    QFile MyFile(filename);
    MyFile.open(QIODevice::ReadOnly);
    QTextStream in (&MyFile);
    QString line;

    if (fmiVer == FMI_1_0)
    {
        modelExchanged = true;
        cosimulation   = false;
    }
    else
    {
        modelExchanged = false;
        cosimulation   = false;
    }

    do {
        line = in.readLine();

        if (fmiVer == FMI_2_0)
        {
            if (line.contains(FMU_MODEL_EXCHANGE, Qt::CaseSensitive)) {
                modelExchanged = true;
            }
            if (line.contains(FMU_COSIMULATION, Qt::CaseSensitive)) {
                cosimulation = true;
            }
        }
        else
        {
            if (line.contains(FMU_IMPLEMENTATION, Qt::CaseSensitive)) {
                modelExchanged = false;
                cosimulation   = true;
            }
        }

    } while (!line.isNull());
    MyFile.close();
}

void opt_model::clearModelData()
{
    types.clear();
    params.clear();
    inputs.clear();
    outputs.clear();
    structure.clear();
}

int  opt_model::findTypeDef(QString name, unsigned type)
{
    bool found = false;
    int  i = 0;

    while (!found && i<types.count())
    {
        found = (types[i].name == name) && (types[i].type == type);
        if (!found) i++;
    }
    return found ? i : -1;
}

void opt_model::getModelInfo(QString path)
{
    QFileInfo     fileinfo;
    QTemporaryDir ext_dir;
    QStringList   files;

    // Open file or resource
    fileinfo.setFile(path + QDir::separator() + FMUfileName);

    // Clear model data
    clearModelData();

    // Auto remove temporal directory
    ext_dir.setAutoRemove(true);

    // Extract FMU file
    files = JlCompress::extractDir(path + QDir::separator() + FMUfileName, ext_dir.path());

    if (files.count()>0)
    {
        // Set model path
        modelPath = ext_dir.path();

        // Set model name
        modelName = fileinfo.baseName();

        // Model description XML file
        QString fxml = modelPath +  QDir::separator() + fModelDescription;

        // Set uri
        uri = modelPath;

        // Model description object
        ModelDescription md(fxml.toStdString().c_str());

        valid = md.isValid() ? FMU_VALID : FMU_XML_ERROR;
        if (valid == FMU_VALID)
        {            
            ModelDescription::Properties p = md.getModelAttributes();

            fmiVer           = md.getVersion();
            if (p.find(FMU_GENERATION_TOOL.toStdString()) != p.not_found())
                vendor           = QString::fromStdString(p.get<std::string>(FMU_GENERATION_TOOL.toStdString()));
            if (p.find(FMU_DATE_TIME.toStdString()) != p.not_found())
            {
                QString date_str = QString::fromStdString(p.get<std::string>(FMU_DATE_TIME.toStdString()));
                date             = date.fromString(date_str,FMI_date_format);
            }
            if (p.find(FMU_GUID.toStdString()) != p.not_found())
                guid             = QString::fromStdString(md.getGUID());
            if (p.find(FMU_VERSION.toStdString()) != p.not_found())
                version          = QString::fromStdString(p.get<std::string>(FMU_VERSION.toStdString()));
            if (p.find(FMU_DESC.toStdString()) != p.not_found())
                desc             = QString::fromStdString(p.get<std::string>(FMU_DESC.toStdString()));
            if (p.find(FMU_COPYRIGHT.toStdString()) != p.not_found())
                copyright        = QString::fromStdString(p.get<std::string>(FMU_COPYRIGHT.toStdString()));
            if (p.find(FMU_LICENSE.toStdString()) != p.not_found())
                license          = QString::fromStdString(p.get<std::string>(FMU_LICENSE.toStdString()));
            if (p.find(FMU_AUTHOR.toStdString()) != p.not_found())
                author           = QString::fromStdString(p.get<std::string>(FMU_AUTHOR.toStdString()));
            if (p.find(FMU_VARIABLENAMING.toStdString()) != p.not_found())
                variableNaming   = QString::fromStdString(p.get<std::string>(FMU_VARIABLENAMING.toStdString()));
            numberOfEvents   = QString::number(md.getNumberOfEventIndicators());

            checkModelExchangeCosimulation(fxml);

            expData = md.hasDefaultExperiment();
            if (expData) md.getDefaultExperiment(expStart,expStop,expTolerance,expStepSize);

            // Type definitions
            if (md.hasTypeDefinitions())
            {
                ModelDescription::Properties mt = md.getTypeDefinitions();
                for (boost::property_tree::ptree::iterator it = mt.begin(); it != mt.end(); it++)
                {
                    ModelDescription::Properties att = ModelDescriptionUtilities::getAttributes(it);
                    ModelDescription::Properties att_type;
                    bool valid = false;

                    QString  name = !att.get_child_optional(FMU_NAME.toStdString()) ? sEmpty : QString::fromStdString(att.get<std::string>(FMU_NAME.toStdString()));
                    unsigned type;

                    if (ModelDescriptionUtilities::hasChild(it,FMU_TXT_REAL.toStdString()) &&
                        ModelDescriptionUtilities::hasChildAttributes(it,FMU_TXT_REAL.toStdString()))
                    {
                        att_type = ModelDescriptionUtilities::getChildAttributes(it,FMU_TXT_REAL.toStdString());
                        type     = FMI_REAL;
                        valid    = true;
                    }
                    else if (ModelDescriptionUtilities::hasChild(it,FMU_TXT_INT.toStdString()) &&
                             ModelDescriptionUtilities::hasChildAttributes(it,FMU_TXT_INT.toStdString()))
                    {
                        att_type = ModelDescriptionUtilities::getChildAttributes(it,FMU_TXT_INT.toStdString());
                        type     = FMI_INTEGER;
                        valid    = true;
                    }
                    else if (ModelDescriptionUtilities::hasChild(it,FMU_TXT_BOOL.toStdString()) &&
                             ModelDescriptionUtilities::hasChildAttributes(it,FMU_TXT_BOOL.toStdString()))
                    {
                        att_type = ModelDescriptionUtilities::getChildAttributes(it,FMU_TXT_BOOL.toStdString());
                        type     = FMI_BOOL;
                        valid    = true;
                    }
                    else if (ModelDescriptionUtilities::hasChild(it,FMU_TXT_STRING.toStdString()) &&
                             ModelDescriptionUtilities::hasChildAttributes(it,FMU_TXT_STRING.toStdString()))
                    {
                        att_type = ModelDescriptionUtilities::getChildAttributes(it,FMU_TXT_STRING.toStdString());
                        type     = FMI_STRING;
                        valid    = true;
                    }

                    if (valid)
                    {
                        typeDefinition td;

                        td.name         = name;
                        td.type         = type;
                        td.unit         = !att_type.get_child_optional(FMU_UNIT.toStdString())     ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_UNIT.toStdString()));
                        td.quantity     = !att_type.get_child_optional(FMU_QUANTITY.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_QUANTITY.toStdString()));
                        td.displayUnit  = !att_type.get_child_optional(FMU_DISUNIT.toStdString())  ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_DISUNIT.toStdString()));
                        td.min          = !att_type.get_child_optional(FMU_MIN.toStdString())      ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_MIN.toStdString()));
                        td.max          = !att_type.get_child_optional(FMU_MAX.toStdString())      ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_MAX.toStdString()));

                        types.append(td);
                    }
                }
            }

            if (md.hasModelVariables())
            {
                // Variables
                ModelDescription::Properties mv = md.getModelVariables();
                for (boost::property_tree::ptree::iterator it = mv.begin(); it != mv.end(); it++)
                {
                    ModelDescription::Properties att = ModelDescriptionUtilities::getAttributes(it);
                    ModelDescription::Properties att_type;

                    QString  name    = !att.get_child_optional(FMU_NAME.toStdString())        ? sEmpty : QString::fromStdString(att.get<std::string>(FMU_NAME.toStdString()));
                    QString  desc    = !att.get_child_optional(FMU_DESCRIPTION.toStdString()) ? sEmpty : QString::fromStdString(att.get<std::string>(FMU_DESCRIPTION.toStdString()));
                    QString  var     = !att.get_child_optional(FMU_VARIABILITY.toStdString()) ? sEmpty : QString::fromStdString(att.get<std::string>(FMU_VARIABILITY.toStdString()));
                    QString  cau     = !att.get_child_optional(FMU_CAUSALITY.toStdString())   ? sEmpty : QString::fromStdString(att.get<std::string>(FMU_CAUSALITY.toStdString()));
                    unsigned type    = FMI_UNDEFINED;
                    int      typeDef = -1;
                    QString  decType = sEmpty;
                    QString  min     = sEmpty;
                    QString  max     = sEmpty;
                    QString  value   = sEmpty;

                    if (ModelDescriptionUtilities::hasChild(it,FMU_TXT_REAL.toStdString()))
                    {
                        type = FMI_REAL;
                        if (ModelDescriptionUtilities::hasChildAttributes(it,FMU_TXT_REAL.toStdString()))
                        {
                            att_type = ModelDescriptionUtilities::getChildAttributes(it,FMU_TXT_REAL.toStdString());
                            decType  = !att_type.get_child_optional(FMU_DEC_TYPE.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_DEC_TYPE.toStdString()));
                            typeDef  = findTypeDef(decType,type);
                            min      = !att_type.get_child_optional(FMU_MIN.toStdString())   ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_MIN.toStdString()));
                            max      = !att_type.get_child_optional(FMU_MAX.toStdString())   ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_MAX.toStdString()));
                            value    = !att_type.get_child_optional(FMU_START.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_START.toStdString()));
                        }
                    }
                    else if (ModelDescriptionUtilities::hasChild(it,FMU_TXT_INT.toStdString()))
                    {
                        type = FMI_INTEGER;
                        if (ModelDescriptionUtilities::hasChildAttributes(it,FMU_TXT_INT.toStdString()))
                        {
                            att_type = ModelDescriptionUtilities::getChildAttributes(it,FMU_TXT_INT.toStdString());
                            decType  = !att_type.get_child_optional(FMU_DEC_TYPE.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_DEC_TYPE.toStdString()));
                            typeDef  = findTypeDef(decType,type);
                            min      = !att_type.get_child_optional(FMU_MIN.toStdString())   ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_MIN.toStdString()));
                            max      = !att_type.get_child_optional(FMU_MAX.toStdString())   ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_MAX.toStdString()));
                            value    = !att_type.get_child_optional(FMU_START.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_START.toStdString()));
                        }
                    }
                    else if (ModelDescriptionUtilities::hasChild(it,FMU_TXT_BOOL.toStdString()))
                    {
                        type = FMI_BOOL;
                        if (ModelDescriptionUtilities::hasChildAttributes(it,FMU_TXT_BOOL.toStdString()))
                        {
                            att_type = ModelDescriptionUtilities::getChildAttributes(it,FMU_TXT_BOOL.toStdString());
                            decType  = !att_type.get_child_optional(FMU_DEC_TYPE.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_DEC_TYPE.toStdString()));
                            typeDef  = findTypeDef(decType,type);
                            value    = !att_type.get_child_optional(FMU_START.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_START.toStdString()));
                        }
                    }
                    else if (ModelDescriptionUtilities::hasChild(it,FMU_TXT_STRING.toStdString()))
                    {
                        type = FMI_STRING;
                        if (ModelDescriptionUtilities::hasChildAttributes(it,FMU_TXT_STRING.toStdString()))
                        {
                            att_type = ModelDescriptionUtilities::getChildAttributes(it,FMU_TXT_STRING.toStdString());
                            decType  = !att_type.get_child_optional(FMU_DEC_TYPE.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_DEC_TYPE.toStdString()));
                            typeDef  = findTypeDef(decType,type);
                            value    = !att_type.get_child_optional(FMU_START.toStdString()) ? sEmpty : QString::fromStdString(att_type.get<std::string>(FMU_START.toStdString()));
                        }
                    }

                    // If not defined in variable get min & max from type definition
                    if (typeDef>=0 && typeDef<types.count())
                    {
                        if (min.isEmpty()) min = types[typeDef].min;
                        if (max.isEmpty()) max = types[typeDef].max;
                    }

                    // Structure storage
                    variable st;

                    st.name    = name;
                    st.desc    = desc;
                    st.type    = type;
                    st.typeDef = typeDef;
                    st.max     = max;
                    st.min     = min;
                    st.isParam = cau == FMU_PARAMETER;
                    st.cau     = cau;
                    st.var     = var;
                    st.value   = value;
                    st.index   = INDEX_UNDEFINED;

                    structure.append(st);


                    // Parameter
                    if ((cau == FMU_PARAMETER))
                    {
                        if (name != sEmpty && type != FMI_UNDEFINED && searchVar(params,name)<0)
                        {
                            variable param;

                            param.name    = name;
                            param.desc    = desc;
                            param.type    = type;
                            param.typeDef = typeDef;
                            param.max     = max;
                            param.min     = min;
                            param.isParam = true;
                            param.value   = value;
                            param.index   = INDEX_UNDEFINED;

                            params.append(param);

                        }
                        //qDebug() << "Parameter: " << name << desc << var << cau << unit << min << max << value;
                    }
                    // Input
                    else if (cau == FMU_INPUT)
                    {
                        if (name != sEmpty && type != FMI_UNDEFINED && searchVar(inputs,name)<0)
                        {
                            variable input;

                            input.name    = name;
                            input.desc    = desc;
                            input.type    = type;
                            input.typeDef = typeDef;
                            input.max     = max;
                            input.min     = min;
                            input.isParam = false;
                            input.value   = value;
                            input.index   = INDEX_UNDEFINED;

                            inputs.append(input);
                        }
                        //qDebug() << "Input: " << name << desc << var << cau << unit << min << max << value;
                    }
                    // Output
                    else if (cau == FMU_OUTPUT)
                    {
                        if (name != sEmpty && type != FMI_UNDEFINED && searchVar(outputs,name)<0)
                        {
                            variable output;

                            output.name    = name;
                            output.desc    = desc;
                            output.type    = type;
                            output.typeDef = typeDef;
                            output.max     = max;
                            output.min     = min;
                            output.isParam = false;
                            output.value   = value;
                            output.index   = INDEX_UNDEFINED;

                            outputs.append(output);
                        }
                        //qDebug() << "Output: " << name << desc << var << cau << unit << min << max << value;
                    }
                }
            }
        }
        else
            valid = FMU_XML_ERROR;
    }
    else
        valid = FMU_ZIP_ERROR;
}
