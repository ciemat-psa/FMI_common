/*
*    This file is part of simint
*
*    Surf - Simulator builder
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef COMMON_TEMPLATE_H
#define COMMON_TEMPLATE_H

#include "formats/read_csv.h"
#include "formats/read_matlab4.h"

template <typename T>
unsigned getDataCSVfile(QString file, QStringList names, QList<QList<T> > &values, QString &error)
{
    struct csv_data *csv_reader;

    error        = sEmpty;
    csv_reader   = read_csv(file.toStdString().c_str());
    if (csv_reader != NULL)
    {
        values.clear();
        for(int i=0;i<names.count();i++)
        {
            double  *csv_vals;
            QList<T> vals;

            csv_vals = read_csv_dataset(csv_reader,names[i].toStdString().c_str());
            if (csv_vals == NULL) return FILE_NO_VAR;

            for(int j=0;j<csv_reader->numsteps;j++)
                vals.append(csv_vals[j]);

            values.append(vals);
        }
        omc_free_csv_reader(csv_reader);
        return FILE_OK;
    }
    return FILE_WRONG_FORMAT;
}

template <typename T>
unsigned getDataMATfile(QString file, QStringList names, QList<QList<T> > &values, QString &error)
{
    ModelicaMatReader mat;

    error = omc_new_matlab4_reader(file.toStdString().c_str(),&mat);
    if (error == 0)
    {
        values.clear();
        for(int i=0;i<names.count();i++)
        {
            ModelicaMatVariable_t *mvar;
            double *mat_vals;
            QList<T> vals;

            mvar = omc_matlab4_find_var(&mat,names[i].toStdString().c_str());
            if (mvar == NULL) return FILE_NO_VAR;

            mat_vals = omc_matlab4_read_vals(&mat,mvar->index);
            if (mat_vals == NULL) return FILE_NO_VAR;

            for(unsigned int j=0;j<mat.nrows;j++)
                vals.append(mat_vals[j]);

            values.append(vals);
        }
        omc_free_matlab4_reader(&mat);
        return FILE_OK;
    }
    return FILE_WRONG_FORMAT;
}

template <typename T>
unsigned getDataFromFile(QString file, unsigned format, QStringList names, QList<QList<T> > &values, QString &error)
{
    // Check file exist and supported format
    unsigned res = checkFileExistAndSupported(file);
    if (res!=FILE_OK) return res;

    // Check file format
    if (format == ffCSV)
        return getDataCSVfile(file,names,values,error);
    else if (format == ffTRJ)
        return getDataMATfile(file,names,values,error);
    else return FILE_UNSUPPORTED;
}

#endif // COMMON_TEMPLATE_H
