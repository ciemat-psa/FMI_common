#include "opt_problem.h"

#include "common.h"
#include "dakota/dakota_methods.h"

// ------------ //
//  opt_problem //
// ------------ //

opt_problem::opt_problem()
{
    // Default method
    QList<opt_method> lmethods;

    getParameterEstimationList(lmethods);
    if (lmethods.count()>0)
        method = lmethods[0];
}

opt_method opt_problem::getMethod() const
{
    return method;
}

void opt_problem::setMethod(const opt_method &value)
{
    method = value;
}

QList<opt_parameter> opt_problem::getParameters() const
{
    return parameters;
}

void opt_problem::setParameters(const QList<opt_parameter> &value)
{
    parameters = value;
}

QList<opt_objfnc> opt_problem::getObjFncs() const
{
    return objFncs;
}

void opt_problem::setObjFncs(const QList<opt_objfnc> &value)
{
    objFncs = value;
}

QList<opt_result> opt_problem::getRes_par() const
{
    return res_par;
}

void opt_problem::setRes_par(const QList<opt_result> &value)
{
    res_par = value;
}

QList<opt_result> opt_problem::getRes_obj() const
{
    return res_obj;
}

void opt_problem::setRes_obj(const QList<opt_result> &value)
{
    res_obj = value;
}

QList<opt_const_eq> opt_problem::getLinear_eq() const
{
    return linear_eq;
}

void opt_problem::setLinear_eq(const QList<opt_const_eq> &value)
{
    linear_eq = value;
}

QList<opt_const_ie> opt_problem::getLinear_ie() const
{
    return linear_ie;
}

void opt_problem::setLinear_ie(const QList<opt_const_ie> &value)
{
    linear_ie = value;
}

QList<opt_const_eq> opt_problem::getNon_linear_eq() const
{
    return non_linear_eq;
}

void opt_problem::setNon_linear_eq(const QList<opt_const_eq> &value)
{
    non_linear_eq = value;
}

QList<opt_const_ie> opt_problem::getNon_linear_ie() const
{
    return non_linear_ie;
}

void opt_problem::setNon_linear_ie(const QList<opt_const_ie> &value)
{
    non_linear_ie = value;
}

QList<opt_result> opt_problem::getRes_con() const
{
    return res_con;
}

void opt_problem::setRes_con(const QList<opt_result> &value)
{
    res_con = value;
}

// -------------- //
//  opt_parameter //
// -------------- //

opt_parameter::opt_parameter(){}

opt_parameter::opt_parameter(QString desc, double value, double lbound, double ubound, bool valid, unsigned sct, double scv)
{
    setDescriptor(desc);
    setInitialValue(value);
    setLowerBound(lbound);
    setUpperBound(ubound);
    setValid(valid);
    setScalingType(sct);
    setScalingValue(scv);
}

QString opt_parameter::getDescriptor() const
{
    return descriptor;
}

void opt_parameter::setDescriptor(const QString &value)
{
    descriptor = value;
}

double opt_parameter::getInitialValue() const
{
    return initialValue;
}

void opt_parameter::setInitialValue(double value)
{
    initialValue = value;
}

double opt_parameter::getLowerBound() const
{
    return lowerBound;
}

void opt_parameter::setLowerBound(double value)
{
    lowerBound = value;
}

double opt_parameter::getUpperBound() const
{
    return upperBound;
}

void opt_parameter::setUpperBound(double value)
{
    upperBound = value;
}

bool opt_parameter::getValid() const
{
    return valid;
}

void opt_parameter::setValid(bool value)
{
    valid = value;
}

unsigned opt_parameter::getScalingType() const
{
    return scalingType;
}

void opt_parameter::setScalingType(const unsigned &value)
{
    scalingType = value;
}

double opt_parameter::getScalingValue() const
{
    return scalingValue;
}

void opt_parameter::setScalingValue(double value)
{
    scalingValue = value;
}

// ----------- //
//  opt_method //
// ----------- //

opt_method::opt_method(){}

opt_method::opt_method(QString m, unsigned i, QString d, unsigned sm, unsigned sg, bool rg, bool rh, unsigned tc, bool ic)
{
    setMethod(m);
    setId(i);
    setDesc(d);
    setSearchMethod(sm);
    setSearchGlobal(sg);
    setRequiresGradient(rg);
    setRequiresHessian(rh);
    setTypeConstraints(tc);
    setInequalityCons(ic);
}

opt_method::~opt_method()
{
}

opt_method &opt_method::operator=(const opt_method& m)
{
    setMethod(m.getMethod());
    setId(m.getId());
    setDesc(m.getDesc());
    setSearchMethod(m.getSearchMethod());
    setSearchGlobal(m.getSearchGlobal());
    setRequiresGradient(m.getRequiresGradient());
    setRequiresHessian(m.getRequiresHessian());
    setTypeConstraints(m.getTypeConstraints());
    setInequalityCons(m.getInequalityCons());

    setArgs(m.getArgs());
    return *this;
}

QString opt_method::getMethod() const
{
    return method;
}

void opt_method::setMethod(const QString &value)
{
    method = value;
}

unsigned opt_method::getId() const
{
    return id;
}

void opt_method::setId(const unsigned &value)
{
    id = value;
}

QString opt_method::getDesc() const
{
    return desc;
}

void opt_method::setDesc(const QString &value)
{
    desc = value;
}

QList<opt_method_arg *> opt_method::getArgs() const
{
    return args;
}

void opt_method::setArgs(const QList<opt_method_arg *> value)
{
    args = value;
}

unsigned opt_method::getSearchMethod() const
{
    return searchMethod;
}

void opt_method::setSearchMethod(const unsigned &value)
{
    searchMethod = value;
}

unsigned opt_method::getSearchGlobal() const
{
    return searchGlobal;
}

void opt_method::setSearchGlobal(const unsigned &value)
{
    searchGlobal = value;
}

bool opt_method::getRequiresGradient() const
{
    return requiresGradient;
}

void opt_method::setRequiresGradient(bool value)
{
    requiresGradient = value;
}

bool opt_method::getRequiresHessian() const
{
    return requiresHessian;
}

void opt_method::setRequiresHessian(bool value)
{
    requiresHessian = value;
}

unsigned opt_method::getTypeConstraints() const
{
    return typeConstraints;
}

void opt_method::setTypeConstraints(const unsigned &value)
{
    typeConstraints = value;
}

bool opt_method::getInequalityCons() const
{
    return inequalityCons;
}

void opt_method::setInequalityCons(bool value)
{
    inequalityCons = value;
}

// --------------- //
//  opt_method_arg //
// --------------- //

opt_method_arg::opt_method_arg(){}

opt_method_arg::opt_method_arg(QString a, QString i, QString d, bool o)
{
    setArgument(a);
    setId(i);
    setDesc(d);
    setOptional(o);
    setChecked(o ? false : true);
    item = NULL;
}

QString opt_method_arg::getArgument() const
{
    return argument;
}

void opt_method_arg::setArgument(const QString &value)
{
    argument = value;
}

bool opt_method_arg::getOptional() const
{
    return optional;
}

void opt_method_arg::setOptional(bool value)
{
    optional = value;
}

QString opt_method_arg::getId() const
{
    return id;
}

void opt_method_arg::setId(const QString &value)
{
    id = value;
}

QString opt_method_arg::getDesc() const
{
    return desc;
}

void opt_method_arg::setDesc(const QString &value)
{
    desc = value;
}

bool opt_method_arg::getChecked() const
{
    return checked;
}

void opt_method_arg::setChecked(bool value)
{
    checked = value;
}

// -------------------- //
//  opt_method_arg_real //
// -------------------- //

double opt_method_arg_real::getValue() const
{
    return value;
}

void opt_method_arg_real::setValue(double v)
{
    value = v;
}

double opt_method_arg_real::getMin() const
{
    return min;
}

void opt_method_arg_real::setMin(double value)
{
    min = value;
}

double opt_method_arg_real::getMax() const
{
    return max;
}

void opt_method_arg_real::setMax(double value)
{
    max = value;
}

double opt_method_arg_real::getDef() const
{
    return def;
}

void opt_method_arg_real::setDef(double value)
{
    def = value;
}

opt_method_arg_real::opt_method_arg_real(QString a, QString i, QString d, bool o, double v, double de, double mi, double ma)
    :opt_method_arg(a,i,d,o)
{
    setType(arg_real);
    setValue(v);
    setDef(de);
    setMin(mi);
    setMax(ma);
}

// ----------------------- //
//  opt_method_arg_integer //
// ----------------------- //

int opt_method_arg_integer::getValue() const
{
    return value;
}

void opt_method_arg_integer::setValue(int v)
{
    value = v;
}

int opt_method_arg_integer::getMin() const
{
    return min;
}

void opt_method_arg_integer::setMin(int value)
{
    min = value;
}

int opt_method_arg_integer::getMax() const
{
    return max;
}

void opt_method_arg_integer::setMax(int value)
{
    max = value;
}

int opt_method_arg_integer::getDef() const
{
    return def;
}

void opt_method_arg_integer::setDef(int value)
{
    def = value;
}

opt_method_arg_integer::opt_method_arg_integer(QString a, QString i, QString d, bool o,  int v, int de, int mi, int ma)
    :opt_method_arg(a,i,d,o)
{
    setType(arg_integer);
    setValue(v);
    setDef(de);
    setMin(mi);
    setMax(ma);
}

// -------------------- //
//  opt_method_arg_bool //
// -------------------- //

opt_method_arg_bool::opt_method_arg_bool(QString a, QString i, QString d, bool o,  bool v, bool de)
    :opt_method_arg(a,i,d,o)
{
    setType(arg_bool);
    setValue(v);
    setDef(de);
}

bool opt_method_arg_bool::getValue() const
{
    return value;
}

void opt_method_arg_bool::setValue(bool v)
{
    value = v;
}

bool opt_method_arg_bool::getDef() const
{
    return def;
}

void opt_method_arg_bool::setDef(bool value)
{
    def = value;
}

// ------------------- //
//  opt_method_arg_set //
// ------------------- //

opt_method_arg_set::opt_method_arg_set(QString a, QString i, QString d, bool o, QString v, QString de, QList<opt_method_arg *>& op)
    :opt_method_arg(a,i,d,o)
{
    setType(arg_subset);
    setValue(v);
    setDef(de);
    setOptions(op);
}

QString opt_method_arg_set::getValue() const
{
    return value;
}

void opt_method_arg_set::setValue(const QString &v)
{
    value = v;
}

QString opt_method_arg_set::getDef() const
{
    return def;
}

void opt_method_arg_set::setDef(const QString &value)
{
    def = value;
}

QList<opt_method_arg *>& opt_method_arg_set::getOptions()
{
    return options;
}

void opt_method_arg_set::setOptions(QList<opt_method_arg *>& value)
{
    options = value;
}

opt_method_arg_string::opt_method_arg_string(QString a, QString i, QString d, bool o,  QString v, QString de)
    :opt_method_arg(a,i,d,o)
{
    setType(arg_string);
    setValue(v);
    setDef(de);
}

QString opt_method_arg_string::getValue() const
{
    return value;
}

void opt_method_arg_string::setValue(QString v)
{
    value = v;
}

QString opt_method_arg_string::getDef() const
{
    return def;
}

void opt_method_arg_string::setDef(const QString &value)
{
    def = value;
}

opt_method_arg_filename::opt_method_arg_filename(QString a, QString i, QString d, bool o,  QString v, QString de, bool op)
    : opt_method_arg_string(a,i,d,o,v,de)
{
    setType(arg_filename);
    setOpen(op);
}

opt_method_arg_keyword::opt_method_arg_keyword(QString a, QString i, QString d, bool o)
 : opt_method_arg(a,i,d,o) {setType(arg_keyword);}

QList<opt_method_arg *> opt_method_arg_struct::getArgs() const
{
    return args;
}

void opt_method_arg_struct::setArgs(const QList<opt_method_arg *> value)
{
    args = value;
}

opt_method_arg_struct::opt_method_arg_struct(QString a, QString i, QString d, bool o, const QList<opt_method_arg *> ar)
    :opt_method_arg(a,i,d,o)
{
    setType(arg_struct);
    setArgs(ar);
}

bool opt_method_arg_filename::getOpen() const
{
    return open;
}

void opt_method_arg_filename::setOpen(bool value)
{
    open = value;
}

QDataStream &operator<<(QDataStream &out, const opt_method &m)
{
    out << m.getMethod() << m.getId() << m.getDesc() << m.getArgs() << m.getSearchMethod() << m.getSearchGlobal() <<
           m.getRequiresGradient() << m.getRequiresHessian() << m.getTypeConstraints() << m.getInequalityCons();
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_method &m)
{
    QString  method;
    unsigned id;
    QString  desc;
    QList<opt_method_arg *> args;
    unsigned searchMethod;
    unsigned searchGlobal;
    bool     requiresGradient;
    bool     requiresHessian;
    unsigned typeConstraints;
    bool     inequalityCons;

    in >> method >> id >> desc >> args >> searchMethod >> searchGlobal >> requiresGradient >> requiresHessian >> typeConstraints >> inequalityCons;

    m.setMethod(method);
    m.setId(id);
    m.setDesc(desc);
    m.setArgs(args);
    m.setSearchMethod(searchMethod);
    m.setSearchGlobal(searchGlobal);
    m.setRequiresGradient(requiresGradient);
    m.setRequiresHessian(requiresHessian);
    m.setTypeConstraints(typeConstraints);
    m.setInequalityCons(inequalityCons);
    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_problem &p)
{
    out << p.getMethod()  << p.getParameters() << p.getObjFncs() << p.getLinear_eq() << p.getLinear_ie() << p.getNon_linear_eq() << p.getNon_linear_ie() <<
           p.getRes_par() << p.getRes_obj()    << p.getRes_con();
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_problem &p)
{
    opt_method           method;
    QList<opt_parameter> params;
    QList<opt_objfnc>    objfuncs;
    QList<opt_const_eq>  linear_eq;
    QList<opt_const_ie>  linear_ie;
    QList<opt_const_eq>  non_linear_eq;
    QList<opt_const_ie>  non_linear_ie;
    QList<opt_result>    res_par;
    QList<opt_result>    res_obj;
    QList<opt_result>    res_con;

    in >> method >> params >> objfuncs >> linear_eq >> linear_ie >> non_linear_eq >> non_linear_ie >> res_par >> res_obj >> res_con;

    p.setMethod(method);
    p.setParameters(params);
    p.setObjFncs(objfuncs);
    p.setLinear_eq(linear_eq);
    p.setLinear_ie(linear_ie);
    p.setNon_linear_eq(non_linear_eq);
    p.setNon_linear_ie(non_linear_ie);
    p.setRes_par(res_par);
    p.setRes_obj(res_obj);
    p.setRes_con(res_con);
    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_const_eq &c)
{
    out << c.a << c.b << c.sct << c.scv;
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_const_eq &c)
{
    in >> c.a >> c.b >> c.sct >> c.scv;
    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_const_ie &c)
{
    out << c.a << c.lb << c.ub << c.has_lb << c.has_ub << c.sct << c.scv;
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_const_ie &c)
{
    in >> c.a >> c.lb >> c.ub >> c.has_lb >> c.has_ub >> c.sct >> c.scv;
    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_objfnc &of)
{
    out << of.getName() << of.getType() << of.getCriteria() << of.getReduction() << of.getUseWeight() << of.getWeight() << of.getScalingType() << of.getScalingValue();
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_objfnc &of)
{
    QString  name;
    unsigned type,cri,red,st;
    bool     uw;
    double   wei,sv;

    in >> name >> type >> cri >> red >> uw >> wei >> st >> sv;

    of.setName(name);
    of.setType(type);
    of.setCriteria(cri);
    of.setReduction(red);
    of.setUseWeight(uw);
    of.setWeight(wei);
    of.setScalingType(st);
    of.setScalingValue(sv);

    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_parameter &p)
{
    out << p.getDescriptor() << p.getInitialValue() << p.getLowerBound() << p.getUpperBound() << p.getValid() << p.getScalingType() << p.getScalingValue();
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_parameter &p)
{
    QString  desc;
    double   val, min, max, scv;
    bool     valid;
    unsigned sct;

    in >> desc >> val >> min >> max >> valid >> sct >> scv;

    p.setDescriptor(desc);
    p.setInitialValue(val);
    p.setLowerBound(min);
    p.setUpperBound(max);
    p.setValid(valid);
    p.setScalingType(sct);
    p.setScalingValue(scv);

    return in;
}

QDataStream &operator<<(QDataStream &out, opt_method_arg *arg)
{
    out << arg->type() << arg->getArgument() << arg->getId() << arg->getChecked() << arg->getDesc() << arg->getOptional();

    switch(arg->type())
    {
        case arg_real:
        {
            opt_method_arg_real *varg_real = (opt_method_arg_real *)arg;
            out << varg_real->getValue() << varg_real->getDef() << varg_real->getMin() << varg_real->getMax();
            break;
        }
        case arg_integer:
        {
            opt_method_arg_integer *varg_int = (opt_method_arg_integer *)arg;
            out << varg_int->getValue() << varg_int->getDef() << varg_int->getMin() << varg_int->getMax();
            break;
        }
        case arg_bool:
        {
            opt_method_arg_bool *varg_bool = (opt_method_arg_bool *)arg;
            out << varg_bool->getValue() << varg_bool->getDef();
            break;
        }
        case arg_string:
        {
            opt_method_arg_string *varg_str = (opt_method_arg_string *)arg;
            out << varg_str->getValue() << varg_str->getDef();
            break;
        }
        case arg_filename:
        {
            opt_method_arg_filename *varg_file = (opt_method_arg_filename *)arg;
            out << varg_file->getValue() << varg_file->getDef() << varg_file->getOpen();
            break;
        }
        case arg_struct:
        {
            opt_method_arg_struct  *varg_str = (opt_method_arg_struct *)arg;
            QList<opt_method_arg *> varg_arg = varg_str->getArgs();

            out << varg_arg.count();
            for(int i=0;i<varg_arg.count();i++)
                out << varg_arg[i];

            break;
        }
        case arg_subset:
        {
            opt_method_arg_set     *varg_set = (opt_method_arg_set *)arg;
            QList<opt_method_arg *> varg_opt = varg_set->getOptions();

            out << varg_set->getValue() << varg_set->getDef() << varg_opt.count();
            for(int i=0;i<varg_opt.count();i++)
                out << varg_opt[i];

            break;
        }
        case arg_keyword: // Nothing additional to save
        {
            break;
        }

    }
    return out;
}

QDataStream &operator>>(QDataStream &in, opt_method_arg *&arg)
{
    unsigned type;
    QString  argument;
    QString  id;
    bool     checked;
    QString  desc;
    bool     optional;

    in >> type >> argument >> id >> checked >> desc >> optional;

    switch(type)
    {
        case arg_real:
        {
            double value,def,min,max;

            in >> value >> def >> min >> max;
            arg = new opt_method_arg_real(argument,id,desc,optional,value,def,min,max);
            arg->setChecked(checked);
            break;
        }
        case arg_integer:
        {
            int value,def,min,max;

            in >> value >> def >> min >> max;
            arg = new opt_method_arg_integer(argument,id,desc,optional,value,def,min,max);
            arg->setChecked(checked);
            break;
        }
        case arg_bool:
        {
            bool value,def;

            in >> value >> def;
            arg = new opt_method_arg_bool(argument,id,desc,optional,value,def);
            arg->setChecked(checked);
            break;
        }
        case arg_string:
        {
            QString value,def;

            in >> value >> def;
            arg = new opt_method_arg_string(argument,id,desc,optional,value,def);
            arg->setChecked(checked);
            break;
        }
        case arg_filename:
        {
            QString value,def;
            bool open;

            in >> value >> def >> open;
            arg = new opt_method_arg_filename(argument,id,desc,optional,value,def,open);
            arg->setChecked(checked);
            break;
        }
        case arg_struct:
        {
            opt_method_arg *sarg;
            QList<opt_method_arg *> args;
            int count;

            in >> count;
            for(int i=0;i<count;i++)
            {
                in >> sarg;
                args.append(sarg);
            }
            arg = new opt_method_arg_struct(argument,id,desc,optional,args);
            arg->setChecked(checked);
            break;
        }
        case arg_subset:
        {
            opt_method_arg *sarg;
            QList<opt_method_arg *> args;
            QString value,def;
            int count;

            in >> value >> def >> count;
            for(int i=0;i<count;i++)
            {
                in >> sarg;
                args.append(sarg);
            }
            arg = new opt_method_arg_set(argument,id,desc,optional,value,def,args);
            arg->setChecked(checked);
            break;
        }
        case arg_keyword:
        {
            arg = new opt_method_arg_keyword(argument,id,desc,optional);
            arg->setChecked(checked);
            break;
        }
    }
    return in;
}

QDataStream &operator<<(QDataStream &out, const QList<opt_method_arg *> &args)
{
    out << args.count();
    for(int i=0;i<args.count();i++)
        out << args[i];
    return out;
}

QDataStream &operator>>(QDataStream &in, QList<opt_method_arg *> &args)
{
    int count;

    in >> count;
    for(int i=0;i<count;i++)
    {
        opt_method_arg *arg;

        in >> arg;
        args.append(arg);
    }
    return in;
}

QDataStream &operator<<(QDataStream &out, const opt_result &r)
{
    out << r.getName() << r.getDesc() << r.getType() << r.getUnit() << r.getValues();

    return out;
}

QDataStream &operator>>(QDataStream &in,  opt_result &r)
{
    QString       name;
    QString       desc;
    unsigned      type;
    QString       unit;
    QList<double> values;

    in >> name >> desc >> type >> unit >> values;

    r.setName(name);
    r.setDesc(desc);
    r.setType(type);
    r.setUnit(unit);
    r.setValues(values);

    return in;
}

opt_objfnc::opt_objfnc(QString n, unsigned t, unsigned crt, unsigned red, bool uw, double w, unsigned st, double sv)
{
    setName(n);
    setType(t);
    setCriteria(crt);
    setReduction(red);
    setUseWeight(uw);
    setWeight(w);
    setScalingType(st);
    setScalingValue(sv);
}

QString opt_objfnc::getName() const
{
    return name;
}

void opt_objfnc::setName(const QString &value)
{
    name = value;
}

unsigned opt_objfnc::getType() const
{
    return type;
}

void opt_objfnc::setType(const unsigned &value)
{
    type = value;
}

unsigned opt_objfnc::getCriteria() const
{
    return criteria;
}

void opt_objfnc::setCriteria(const unsigned &value)
{
    criteria = value;
}

QString opt_objfnc::getCriteriaText()
{
    switch (criteria)
    {
    case crtMinimize: return crttMinimize;
    case crtMaximize: return crttMaximize;
    default:          return crttMinimize;
    }
}

unsigned opt_objfnc::getReduction() const
{
    return reduction;
}

void opt_objfnc::setReduction(const unsigned &value)
{
    reduction = value;
}

bool opt_objfnc::getUseWeight() const
{
    return useWeight;
}

void opt_objfnc::setUseWeight(bool value)
{
    useWeight = value;
}

double opt_objfnc::getWeight() const
{
    return weight;
}

void opt_objfnc::setWeight(double value)
{
    weight = value;
}

unsigned opt_objfnc::getScalingType() const
{
    return scalingType;
}

void opt_objfnc::setScalingType(const unsigned &value)
{
    scalingType = value;
}

double opt_objfnc::getScalingValue() const
{
    return scalingValue;
}

void opt_objfnc::setScalingValue(double value)
{
    scalingValue = value;
}

opt_result::opt_result(QString n, unsigned t, QString d, QString u)
{
    setName(n);
    setType(t);
    setDesc(d);
    setUnit(u);
}

QString opt_result::getName() const
{
    return name;
}

void opt_result::setName(const QString &value)
{
    name = value;
}

unsigned opt_result::getType() const
{
    return type;
}

void opt_result::setType(const unsigned &value)
{
    type = value;
}

QString opt_result::getDesc() const
{
    return desc;
}

void opt_result::setDesc(const QString &value)
{
    desc = value;
}

QString opt_result::getUnit() const
{
    return unit;
}

void opt_result::setUnit(const QString &value)
{
    unit = value;
}

QList<double> opt_result::getValues() const
{
    return values;
}

void opt_result::setValues(const QList<double> &value)
{
    values = value;
}

void opt_result::setValue(const double &value)
{
    values.append(value);
}

void opt_result::clearValues()
{
    values.clear();
}

QString getScalingTypeText(unsigned scalingType)
{
    switch (scalingType)
    {
        case scNone:  return sctNone;
        case scValue: return sctValue;
        case scAuto:  return sctAuto;
        case scLog:   return sctLog;
        default:      return sctNone;
    }
}
