#include "fmuinterface.h"

#include "opt_problem.h"
#include "common.h"
#include "common/fmi_v1.0/fmiModelTypes.h"
#include <iostream>

namespace SIM{

const int FMUInt_OK   = 0;
const int FMUInt_STOP = 1;

int findVariable(QList<variable> vars, QString name)
{
    bool found = false;
    int  i     = 0;

    while (!found && i<vars.count())
    {
        found = vars[i].name == name;
        if (!found) i++;
    }
    return found ? i : -1;
}

FMUInterface::FMUInterface(const Dakota::ProblemDescDB &problem_db, opt_project *prj)
    :DirectApplicInterface(problem_db)
{
    opt_exp exp = prj->getExp();

    // Set there is experimental data
    hasExp = true;

    // Build experimental data
    exp.buildData(prj->getModel().inputs,prj);
    prj->setExp(exp);

    // Set project
    sim.setProject(prj);

    // Initialize stop simulation
    stopSim = false;
}

void FMUInterface::setProject(opt_project *prj)
{
    sim.setProject(prj);
}

opt_project *FMUInterface::getProject()
{
    return sim.getProject();
}

void FMUInterface::stop()
{
    stopSim = true;
}

bool FMUInterface::wasStopped()
{
    return stopSim;
}

bool FMUInterface::getExp(QList<opt_result> par, opt_exp &exp)
{
    for(int i=0;i<lexp.count();i++)
    {
        bool found;
        int  j = 0;

        do{
            //qDebug() << lpar[i][j] << " , " << par[j].getValues()[0];
            found = lpar[i][j] == par[j].getValues()[0];
            j++;
        }while(found && j<lpar[i].count());
        if (found)
        {
            exp = lexp[i];
            return true;
        }
    }
    return false;
}

void FMUInterface::clearListExp()
{
    lexp.clear();
    lpar.clear();
}

void FMUInterface::clearPreobj()
{
    pre_obj.clear();
}

void FMUInterface::setFMUParameters()
{
    opt_project          *prj = sim.getProject();
    QList<opt_parameter>  par = prj->getPrb().getParameters();
    opt_model             mo  = prj->getModel();
    QList<double>         p;

    // Set parameter values according to Dakota
    for(int i=0;i<xC.length();i++)
    {
       int     index = mo.p_name_new.indexOf(par[i].getDescriptor());
       QString spVal = QString::number(xC[i]);

       p.append(xC[i]);

       if (index>=0)
       {
        mo.p_value_new[index] = spVal;
        //qDebug() << endl << mo.p_name_new[index]<< " = " << mo.p_value_new[index] << endl;
       }
       else
       {
           mo.p_name_new.append(par[i].getDescriptor());
           mo.p_value_new.append(spVal);
           //qDebug() << endl << mo.p_name_new.last() << " = " << mo.p_value_new.last() << endl;
       }
    }

    // Add pararameters to list only
    // for input estimation
    if (prj->getType() == ptInputEst)
        lpar.append(p);

    // Set parameters in project
    prj->setModel(mo);
    //sim.setProject(prj);
}

double sumTrjSquare(QList<double> values)
{
    double rms=0;

    for(int i=0;i<values.count();i++)
        rms += pow(values[i],2);

    return sqrt(rms/values.count());
}

double sumTrjAbs(QList<double> values)
{
    double sum=0;

    for(int i=0;i<values.count();i++)
        sum += qFabs(values[i]);

    return sum/values.count();
}

double maxTrj(QList<double> values)
{
    return *std::max_element(values.begin(), values.end());
}

double minTrj(QList<double> values)
{
    return *std::min_element(values.begin(), values.end());
}

double lastTrj(QList<double> values)
{
    return values.last();
}


double trajectoryReduction(QList<double> values, unsigned reduc)
{
    switch(reduc)
    {
        case redRMS:  return sumTrjSquare(values);
        case redSum:  return sumTrjAbs(values);
        case redLast: return lastTrj(values);
        case redMax:  return maxTrj(values);
        case redMin:  return minTrj(values);
        default:      return sumTrjSquare(values);
    }
}

double nonLinearConstraintReduction(QList<double> values, opt_const_eq c)
{
    for(int i=0;i<values.count();i++)
    {
        if (values[i]!=c.b)
            return values[i];
    }
    return values.last();
}

double nonLinearConstraintReduction(QList<double> values, opt_const_ie c)
{
    for(int i=0;i<values.count();i++)
    {
        if (values[i]<c.lb || values[i]>c.ub)
            return values[i];
    }
    return values.last();
}

void FMUInterface::setStopFMUResults()
{
    opt_project        *prj = sim.getProject();
    opt_problem         prb = prj->getPrb();
    QList<opt_objfnc>   obj = prb.getObjFncs();
    QList<opt_const_eq> ceq = prb.getNon_linear_eq();
    QList<opt_const_ie> cie = prb.getNon_linear_ie();
    int                 i0;

    // Order:
    // ------
    // 1.- Objective functions
    // 2.- Inequality constraints
    // 3.- Equality constraints
    //

    // Objective functions
    for(int i=0;i<obj.count();i++)
    {
        fnVals[i] = obj[i].getCriteria() == crtMinimize ? std::numeric_limits<double>::max() :
                                                          -DBL_MAX; // When no c++11 suport
                                                          //std::numeric_limits<double>::lowest();
        // Store previuos results and apply filter
        if (pre_obj.count()>i)
        {
            const double LPF_beta = 0.1; // [0,1]
            fnVals[i]  = LPF_beta * fnVals[i] + (1-LPF_beta) * pre_obj[i];
            pre_obj[i] = fnVals[i];
        }
        else
            pre_obj.append(fnVals[i]);
    }

    // Inequality constraints
    i0 = obj.count();
    for(int i=0;i<cie.count();i++)
        fnVals[i0+i] = 0;

    // Equality constraints
    i0 = obj.count() + cie.count();
    for(int i=0;i<ceq.count();i++)
        fnVals[i0+i] = 0;

    // Delete dynamic objects
    //delete prj;
}


void FMUInterface::setFMUResults()
{
    opt_project        *prj = sim.getProject();
    opt_model           mo  = prj->getModel();
    opt_problem         prb = prj->getPrb();
    QList<opt_objfnc>   obj = prb.getObjFncs();
    QList<opt_const_eq> ceq = prb.getNon_linear_eq();
    QList<opt_const_ie> cie = prb.getNon_linear_ie();
    opt_exp             exp = prj->getExp();
    int                 i0;

    // Order:
    // ------
    // 1.- Objective functions
    // 2.- Inequality constraints
    // 3.- Equality constraints
    //

    //#pragma omp parallel sections
    {

    //#pragma omp section
    {
        // Objective functions
        //#pragma omp parallel for
        for(int i=0;i<obj.count();i++)
        {
            // Find objective function in outputs
            int index = findVariable(mo.outputs,obj[i].getName());

            // If found set value
            if (index>-1)
            {
                fnVals[i] = trajectoryReduction(exp.values_output[index],obj[i].getReduction());
                //qDebug() << "fnVals[" << i << "] = " << fnVals[i];
            }
        }
    }

    //#pragma omp section
    {
        // Inequality constraints
        i0 = obj.count();
        //#pragma omp parallel for
        for(int i=0;i<cie.count();i++)
        {
            // Find nonlinear constraint
            int index = findSelItem(cie[i]);

            // If found set value
            if (index>-1)
            {
                fnVals[i0+i] = nonLinearConstraintReduction(exp.values_output[index],cie[i]);
                //qDebug() << "fnVals[" << i << "] = " << fnVals[i0+i];
            }
        }
    }

    //#pragma omp section
    {
        // Equality constraints
        i0 = obj.count() + cie.count();
        //#pragma omp parallel for
        for(int i=0;i<ceq.count();i++)
        {
            // Find nonlinear constraint
            int index = findSelItem(ceq[i]);

            // If found set value
            if (index>-1)
            {
                fnVals[i0+i] = nonLinearConstraintReduction(exp.values_output[index],ceq[i]);
                //qDebug() << "fnVals[" << i << "] = " << fnVals[i0+i];
            }
        }
    }

    }

    //delete prj;
}

int FMUInterface::derived_map_ac(const Dakota::String &ac_name)
{
    Q_UNUSED(ac_name);

    // Process app events
    QCoreApplication::processEvents();

    // If no stop requested
    if (!stopSim)
    {
        // Set design variables from Dakota in FMU
        setFMUParameters();

        // Simulate
        sim.run(false,!hasExp);

        if (sim.getStatus() == fmiOK)
        {
            // Set simulation results from FMU in Dakota
            setFMUResults();

            // Store all parameters and results to find values
            // for initialization in input estimation
            if(sim.getProjectType() == ptInputEst)
            {
                opt_project *p = sim.getProject();

                lexp.append(p->getExp());
                //delete p;
            }
        }
        else
        {
            // Set dummy values
            setStopFMUResults();
        }

        // Emit update signal
        emit update();

        // Return statement
        return FMUInt_OK;
    }
    // Stopped optimization

    // WARNING: caught by JEGA, so not throw to this app
    // throw std::bad_exception();

    // Set dummy values
    setStopFMUResults();

    return FMUInt_STOP;
}

}
