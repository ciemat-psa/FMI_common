#ifndef DAKOTAMETHODS_H
#define DAKOTAMETHODS_H

#include "opt_problem.h"

// Parameter estimation algorithms id

void getParameterEstimationList(QList<opt_method> &lmethods);

#endif // DAKOTAMETHODS_H
