#ifndef FMUINTERFACE_H
#define FMUINTERFACE_H

// ---------------------------------------------- //
// WARNING: Is necessary to define this manually? //
// ---------------------------------------------- //
#ifdef __linux__
#define DAKOTA_HAVE_MPI 1
#endif
// ---------------------------------------------- //
#include <QtCore>

#include "opt_project.h"
#include "simulation.h"

#include "warnings_off.h"
#include "LibraryEnvironment.hpp"
#include "ParallelLibrary.hpp"
#include "DakotaModel.hpp"
#include "DakotaInterface.hpp"
#include "DakotaResponse.hpp"
#include "ProblemDescDB.hpp"
#include "DirectApplicInterface.hpp"
#include "PluginSerialDirectApplicInterface.hpp"
#include "PluginParallelDirectApplicInterface.hpp"
#include "OutputManager.hpp"
#include "warnings_on.h"

using namespace Dakota;

namespace SIM{

    class FMUInterface: public QObject, public Dakota::DirectApplicInterface
    {
        Q_OBJECT

    public:
            FMUInterface(const Dakota::ProblemDescDB &problem_db, opt_project *p);
            void setProject(opt_project *prj);
            opt_project *getProject();
            bool wasStopped();
            bool getExp(QList<opt_result> par, opt_exp &exp);
            void clearListExp();
            void clearPreobj();

    protected:
            int derived_map_ac(const Dakota::String &ac_name);
            //int derived_map_if(const Dakota::String &if_name);
            //int derived_map_of(const Dakota::String &of_name);

        private:
            simulation            sim;
            bool                  stopSim;
            bool                  hasExp;

            // List of parameters and experiment results
            QList<opt_exp>        lexp;
            QList<QList<double> > lpar;

            // Previous objective functions
            QList<double> pre_obj;

            void setFMUParameters();
            void setFMUResults();
            void setStopFMUResults();

        public slots:
            void stop();

        signals:
            void update();
    };

}

#endif // FMUINTERFACE_H
