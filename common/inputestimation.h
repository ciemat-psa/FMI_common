#ifndef INPUTESTIMATION_H
#define INPUTESTIMATION_H

#include <QtCore>

#include "dakota/fmuinterface.h"
#include "opt_project.h"
#include "common.h"

class inputEstimation: public QObject
{
    Q_OBJECT

private:
    // Variables -------------------
    opt_project *prj;
    int          t_pe;
    fmiTime      time;
    QString      fileOut;
    QString      fileError;
    QString      dakota_log_level;

public:
    inputEstimation(opt_project *p);
    ~inputEstimation();
    int getTime();
    opt_project *getProject();
    QString getOutFile();
    QString getErrorFile();
    void requestStop();
    QString getDakota_log_level() const;
    void setDakota_log_level(const QString &value);
    void setInitialConditions(opt_model &mo, opt_exp exp);
    void limitVariation(LibraryEnvironment &env, QList<opt_result>pars);

public slots:
    void run();
    void update();

signals:
    void finished(double t, opt_exp exp, opt_problem prb, unsigned status);
    void end();
    void inputEstUpdate(double t, double tstart, double tstop);
    void inputEstError(unsigned s, QString msg);
    void stop();
};

#endif // INPUTESTIMATION_H
