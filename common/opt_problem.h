#ifndef OPT_PROBLEM_H
#define OPT_PROBLEM_H

#include <QtCore>

const unsigned smGradient = 1;
const unsigned smFreeDer  = 2;

const unsigned sgGlobal   = 1;
const unsigned sgLocal    = 2;

const unsigned tcUnconstraint = 1;
const unsigned tcBound        = 2;
const unsigned tcLinear       = 3;
const unsigned tcNonLinear    = 4;

const unsigned arg_generic  = 1;
const unsigned arg_keyword  = 2;
const unsigned arg_struct   = 3;
const unsigned arg_real     = 4;
const unsigned arg_integer  = 5;
const unsigned arg_bool     = 6;
const unsigned arg_string   = 7;
const unsigned arg_filename = 8;
const unsigned arg_subset   = 9;

const unsigned crtMinimize  = 0;
const unsigned crtMaximize  = 1;

const QString crttMinimize = "Minimize";
const QString crttMaximize = "Maximize";

const unsigned redRMS  = 0;
const unsigned redSum  = 1;
const unsigned redLast = 2;
const unsigned redMax  = 3;
const unsigned redMin  = 4;

const QString redtRMS  = "Root mean square";
const QString redtSum  = "Absolute mean";
const QString redtLast = "Last value";
const QString redtMax  = "Max value";
const QString redtMin  = "Min value";

// Scaling options
const unsigned scNone  = 0;
const unsigned scValue = 1;
const unsigned scLog   = 2;
const unsigned scAuto  = 3;

const QString sctNone  = "None";
const QString sctValue = "Value";
const QString sctLog   = "Log";
const QString sctAuto  = "Auto";

class opt_parameter
{
public:
    opt_parameter();
    opt_parameter(QString desc, double value, double lbound, double ubound, bool valid, unsigned sct, double scv);
    QString  getDescriptor() const;
    void     setDescriptor(const QString &value);
    double   getInitialValue() const;
    void     setInitialValue(double value);
    double   getLowerBound() const;
    void     setLowerBound(double value);
    double   getUpperBound() const;
    void     setUpperBound(double value);
    bool     getValid() const;
    void     setValid(bool value);
    unsigned getScalingType() const;
    void     setScalingType(const unsigned &value);
    double   getScalingValue() const;
    void     setScalingValue(double value);

private:
    QString  descriptor;
    double   initialValue;
    double   lowerBound;
    double   upperBound;
    bool     valid;
    unsigned scalingType;
    double   scalingValue;
};

class opt_method_arg
{
public:
    opt_method_arg();
    opt_method_arg(QString a, QString i, QString d, bool o);
    QString getArgument() const;
    void setArgument(const QString &value);
    bool getOptional() const;
    void setOptional(bool value);
    QString getId() const;
    void setId(const QString &value);
    QString getDesc() const;
    void setDesc(const QString &value);
    bool getChecked() const;
    void setChecked(bool value);
    unsigned type(){return type_arg;}
    void setType(unsigned t){type_arg=t;}

    // WARNING: This must be changed
    // for linking purposes in GUI
    void *item;
private:
    unsigned type_arg;
    QString  argument;
    QString  id;
    QString  desc;
    bool     optional;
    bool     checked;
};

class opt_method_arg_keyword : public opt_method_arg
{
public:
    opt_method_arg_keyword(QString a, QString i, QString d, bool o);
};

class opt_method_arg_struct : public opt_method_arg
{
public:
    opt_method_arg_struct(QString a, QString i, QString d, bool o, const QList<opt_method_arg *> args);
    QList<opt_method_arg *> getArgs() const;
    void setArgs(const QList<opt_method_arg *> value);
private:
    QList<opt_method_arg *> args;
};

class opt_method_arg_real : public opt_method_arg
{
public:
    opt_method_arg_real(QString a, QString i, QString d, bool o,  double v, double de, double mi, double ma);
    double getValue() const;
    void setValue(double value);
    double getMin() const;
    void setMin(double value);
    double getMax() const;
    void setMax(double value);
    unsigned type(){return arg_real;}
    double getDef() const;
    void setDef(double value);
private:
    double value;
    double def;
    double min;
    double max;
};

class opt_method_arg_integer : public opt_method_arg
{
public:
    opt_method_arg_integer(QString a, QString i, QString d, bool o,  int v, int de, int mi, int ma);
    int getValue() const;
    void setValue(int value);
    int getMin() const;
    void setMin(int value);
    int getMax() const;
    void setMax(int value);
    unsigned type(){return arg_integer;}
    int getDef() const;
    void setDef(int value);

private:
    int value;
    int def;
    int min;
    int max;
};

class opt_method_arg_bool : public opt_method_arg
{
public:
    opt_method_arg_bool(QString a, QString i, QString d, bool o,  bool v, bool de);
    bool getValue() const;
    void setValue(bool value);
    unsigned type(){return arg_bool;}
    bool getDef() const;
    void setDef(bool value);

private:
    bool value;
    bool def;
};

class opt_method_arg_string : public opt_method_arg
{
public:
    opt_method_arg_string(QString a, QString i, QString d, bool o,  QString v, QString de);
    QString getValue() const;
    void setValue(QString v);
    unsigned type(){return arg_string;}
    QString getDef() const;
    void setDef(const QString &value);

private:
    QString value;
    QString def;
};

class opt_method_arg_filename : public opt_method_arg_string
{
    bool open;

public:
    opt_method_arg_filename(QString a, QString i, QString d, bool o,  QString v, QString de, bool op);
    unsigned type(){return arg_filename;}
    bool getOpen() const;
    void setOpen(bool value);
};

class opt_method_arg_set : public opt_method_arg
{
public:
    opt_method_arg_set(QString a, QString i, QString d, bool o, QString v, QString de, QList<opt_method_arg *>& op);
    QString getValue() const;
    void setValue(const QString &value);
    QString getDef() const;
    void setDef(const QString &value);
    QList<opt_method_arg *>& getOptions();
    void setOptions(QList<opt_method_arg *> &value);
private:
    QString value;
    QString def;
    QList<opt_method_arg *> options;

};

class opt_method
{
public:
    opt_method();
    opt_method(QString m, unsigned i, QString d, unsigned sm, unsigned sg, bool rg, bool rh, unsigned tc, bool ic);
    ~opt_method();
    QString  getMethod() const;
    void     setMethod(const QString &value);
    unsigned getId() const;
    void     setId(const unsigned &value);
    QString  getDesc() const;
    void     setDesc(const QString &value);
    QList<opt_method_arg *> getArgs() const;
    void     setArgs(const QList<opt_method_arg *> value);

    unsigned getSearchMethod() const;
    void setSearchMethod(const unsigned &value);
    unsigned getSearchGlobal() const;
    void setSearchGlobal(const unsigned &value);
    bool getRequiresGradient() const;
    void setRequiresGradient(bool value);
    bool getRequiresHessian() const;
    void setRequiresHessian(bool value);
    unsigned getTypeConstraints() const;
    void setTypeConstraints(const unsigned &value);
    bool getInequalityCons() const;
    void setInequalityCons(bool value);

    opt_method &operator=( const opt_method& m);

private:
    QString  method;
    unsigned id;
    QString  desc;
    QList<opt_method_arg *> args;

    unsigned searchMethod;
    unsigned searchGlobal;
    bool     requiresGradient;
    bool     requiresHessian;
    unsigned typeConstraints;
    bool     inequalityCons;
};

class opt_objfnc
{
public:
    opt_objfnc(){}
    opt_objfnc(QString n, unsigned t, unsigned crt, unsigned red, bool uw, double w, unsigned st, double sv);
    QString getName() const;
    void setName(const QString &value);
    unsigned getType() const;
    void setType(const unsigned &value);
    unsigned getCriteria() const;
    void setCriteria(const unsigned &value);
    unsigned getReduction() const;
    void setReduction(const unsigned &value);
    bool getUseWeight() const;
    void setUseWeight(bool value);
    double getWeight() const;
    void setWeight(double value);
    unsigned getScalingType() const;
    void setScalingType(const unsigned &value);
    double getScalingValue() const;
    void setScalingValue(double value);
    QString getCriteriaText();

private:
    QString  name;
    unsigned type;
    unsigned criteria;
    unsigned reduction;
    bool     useWeight;
    double   weight;
    unsigned scalingType;
    double   scalingValue;
};

class opt_result
{

public:
    opt_result(){}
    opt_result(QString n, unsigned t, QString d, QString u);
    QString  getName() const;
    void     setName(const QString &value);
    unsigned getType() const;
    void     setType(const unsigned &value);
    QString  getDesc() const;
    void     setDesc(const QString &value);
    QString  getUnit() const;
    void     setUnit(const QString &value);
    QList<double> getValues() const;
    void     setValues(const QList<double> &value);
    void     setValue(const double &value);
    void     clearValues();

private:
    QString       name;
    unsigned      type;
    QString       desc;
    QString       unit;
    QList<double> values;
};

class opt_const_eq
{
    public:
        opt_const_eq(){}
        opt_const_eq(int size_a)
        {
            b   = 0;
            sct = 0;
            scv = 0;
            for(int i=0;i<size_a;i++)
                a.append(0);
        }
        QList<double> a;
        double        b;
        bool          valid;
        unsigned      sct;
        double        scv;
};

class opt_const_ie
{
    public:
        opt_const_ie(){}
        opt_const_ie(int size_a)
        {
            has_lb = false;
            has_ub = false;
            lb     = 0;
            ub     = 0;
            sct    = 0;
            scv    = 0;
            for(int i=0;i<size_a;i++)
                a.append(0);
        }
        QList<double> a;
        bool          has_lb;
        bool          has_ub;
        double        lb;
        double        ub;
        bool          valid;
        unsigned      sct;
        double        scv;
};


class opt_problem
{

public:
    opt_problem();
    opt_method getMethod() const;
    void setMethod(const opt_method &value);
    QList<opt_parameter> getParameters() const;
    void setParameters(const QList<opt_parameter> &value);
    QList<opt_objfnc> getObjFncs() const;
    void setObjFncs(const QList<opt_objfnc> &value);
    QList<opt_result> getRes_par() const;
    void setRes_par(const QList<opt_result> &value);
    QList<opt_result> getRes_obj() const;
    void setRes_obj(const QList<opt_result> &value);
    QList<opt_result> getRes_con() const;
    void setRes_con(const QList<opt_result> &value);
    QList<opt_const_eq> getLinear_eq() const;
    void setLinear_eq(const QList<opt_const_eq> &value);
    QList<opt_const_ie> getLinear_ie() const;
    void setLinear_ie(const QList<opt_const_ie> &value);
    QList<opt_const_eq> getNon_linear_eq() const;
    void setNon_linear_eq(const QList<opt_const_eq> &value);
    QList<opt_const_ie> getNon_linear_ie() const;
    void setNon_linear_ie(const QList<opt_const_ie> &value);

private:
    // Parameter list
    QList<opt_parameter> parameters;

    // Optimization method
    opt_method method;

    // Fitness function
    QList<opt_objfnc> objFncs;

    // Constraints
    QList<opt_const_eq> linear_eq;
    QList<opt_const_ie> linear_ie;
    QList<opt_const_eq> non_linear_eq;
    QList<opt_const_ie> non_linear_ie;

    // Results
    QList<opt_result> res_par;
    QList<opt_result> res_obj;
    QList<opt_result> res_con;
};

QDataStream &operator>>(QDataStream &in,  QList<opt_method_arg *> &args);
QDataStream &operator<<(QDataStream &out, const QList<opt_method_arg *> &args);
QDataStream &operator>>(QDataStream &in,  opt_method_arg *&arg);
QDataStream &operator<<(QDataStream &out, opt_method_arg *arg);
QDataStream &operator<<(QDataStream &out, const opt_problem &p);
QDataStream &operator>>(QDataStream &in,  opt_problem &p);
QDataStream &operator<<(QDataStream &out, const opt_const_eq &c);
QDataStream &operator<<(QDataStream &out, const opt_const_ie &c);
QDataStream &operator>>(QDataStream &in, opt_const_eq &c);
QDataStream &operator>>(QDataStream &in, opt_const_ie &c);
QDataStream &operator<<(QDataStream &out, const opt_result &r);
QDataStream &operator>>(QDataStream &in,  opt_result &r);

QString getScalingTypeText(unsigned scalingType);

#endif // OPT_PROBLEM_H
