#include "parest.h"
#include "common.h"
#include "opt_project.h"

#include <iostream>
#ifdef __linux__
#include <mpi.h>
#endif

const QString DAKOTA_THROW      = "throw";
const QString DAKOTA_FMU_PLUGIN = "plugin_fmu";
const QString DAKOTA_DIRECT     = "direct";
const QString DAKOTA_NONE       = "none";
const QString DAKOTA_FILE_OUT   = "optimization_out.txt";
const QString DAKOTA_FILE_ERROR = "optimization_error.txt";
const QString DAKOTA_FLAT_FILE  = "flat_file";

const std::string model_type(sEmpty.toStdString()); // Empty string will match any model type
const std::string interf_type(DAKOTA_DIRECT.toStdString());
const std::string an_driver(DAKOTA_FMU_PLUGIN.toStdString());

unsigned runParEst(opt_project *p, QString dakota_log_level)
{
    // Variables --------------------------
    opt_project *prj = NULL;
    QString      fileOut;
    QString      fileError;
    QTime        timer;
    int          t_pe;
    unsigned     status = DAKOTA_OK;
    // Log info ---------------------------
    QStringList     log;
    QList<unsigned> ltype;
    QStringList     ltime;
    // ------------------------------------

    // Start timer
    timer.start();

    // Copy project
    copyProject(p,&prj);

    // Names for output and error files
    setFileOutError(fileOut,fileError);

    // Dakota options
    Dakota::ProgramOptions opts;

    // Output and error file
    opts.output_file(fileOut.toStdString());
    opts.error_file(fileError.toStdString());

    // Delay validation/sync of the Dakota database and iterator
    // construction to allow update after all data is populated
    bool check_bcast_construct = false;

    // Dakota library
    Dakota::LibraryEnvironment env(MPI_COMM_WORLD,opts,check_bcast_construct);

    // Simulator interface
    SIM::FMUInterface *FMU_int = NULL;

    try{
        // Extract FMU file
        extractFMUdir(prj);

        // Define ProblemDB in Dakota
        buildDakotaProblemDB(prj,dakota_log_level,env);

        // FMU interface
        FMU_int = serial_interface_plugin(prj,env);

        // Wait for all processes had built their environments
        #ifdef __linux__
        MPI_Barrier(MPI_COMM_WORLD);
        #endif

        // Log start optimization
        sendToLog(log,ltype,ltime,m_startingParEst);

        // Execute parameter estimation
        env.execute();

        // Problem status
        status = FMU_int == NULL ? DAKOTA_ERROR : FMU_int->wasStopped() ? DAKOTA_STOPPED : DAKOTA_OK;

        // If there is an error send to log
        if (status == DAKOTA_ERROR) sendToLog(log,ltype,ltime,m_error,LOG_ERROR);

        // Simulation finished
        t_pe = timer.elapsed();

        // Simulation finished and time
        sendToLog(log,ltype,ltime,m_parEstFinished);
        sendToLog(log,ltype,ltime,m_executionTime + t_space + msToTime(t_pe));

        // Wait for all processes to gather results
        #ifdef __linux__
        MPI_Barrier(MPI_COMM_WORLD);
        #endif

        // Save results
        if (env.parallel_library().world_rank() == 0)
        {
            // Save results to project
            saveResults(&prj,env,env.problem_description_db());

            // Save log information
            prj->setLmesg(log);
            prj->setLtype(ltype);
            prj->setLtime(ltime);

            // Save project to file
            prj->save_to_file();
        }

        // Delete FMU dir
        deleteDir(prj->getModel().uri);

    }catch(std::exception& e)
    {Q_UNUSED(e);}

    // Delete dynamic objects
    deleteProject(&prj);
    //deleteFMUInterface(&FMU_int); WARNING: silent- JEGA Front End: signal caught: value = 11 (SIGSEGV)

    return status;
}

void buildDakotaProblemDB(opt_project *prj, QString dakota_log_level, Dakota::LibraryEnvironment &env)
{
    // Dakota structures -----------
    Dakota::DataMethod      dme;
    Dakota::DataModel       dmo;
    Dakota::DataVariables   dv;
    Dakota::DataInterface   di;
    Dakota::DataResponses   dr;

    // configure Dakota to throw a std::runtime_error instead of calling exit
    env.exit_mode();//DAKOTA_THROW.toStdString());

    // Now set the various data to specify the Dakota study only on rank 0
    Dakota::ParallelLibrary &parallel_lib = env.parallel_library();
    if (parallel_lib.world_rank() == 0)
    {
        // Set method, model, variables, interface and responses
        dme = setDakotaMethod(prj,dakota_log_level);
        dmo = setDakotaModel();
        dv  = setDakotaVariables(prj);
        di  = setDakotaInterface();
        dr  = setDakotaResponses(prj);

        // Insert in database
        env.insert_nodes(dme, dmo, dv, di, dr);
    }

    // Once done with changes: check database, broadcast, and construct iterators
    env.done_modifying_db();

    // ------------------------- //
    // WARNING: check this usage //
    // ------------------------- //
    // plug the client's interface (function evaluator) into the Dakota
    // environment; in serial case, demonstrate the simpler plugin method
    //if (env->mpi_manager().mpirun_flag())
    //  parallel_interface_plugin(*env);
    //else
    //  serial_interface_plugin();
}

void copyProject(opt_project *source, opt_project **dest)
{
    if (source != NULL)
    {
        // New project
        *dest = new opt_project(ptNoType);

        // Copy project
        **dest = *source;
    }
}

Dakota::DataMethod setDakotaMethod(opt_project *prj, QString dakota_log_level)
{
    Dakota::DataMethod     dme;
    Dakota::DataMethodRep *dmer;
    opt_method method = prj->getPrb().getMethod();

    dmer               = dme.data_rep();
    dmer->methodName   = method.getId();

    if      (dakota_log_level == DAKOTA_QUIET)   dmer->methodOutput = Dakota::QUIET_OUTPUT;
    else if (dakota_log_level == DAKOTA_SILENT)  dmer->methodOutput = Dakota::SILENT_OUTPUT;
    else if (dakota_log_level == DAKOTA_VERBOSE) dmer->methodOutput = Dakota::VERBOSE_OUTPUT;
    else if (dakota_log_level == DAKOTA_DEBUG)   dmer->methodOutput = Dakota::DEBUG_OUTPUT;
    else                                         dmer->methodOutput = Dakota::NORMAL_OUTPUT;

    // ------------------------------- //
    // Additional method configuration //
    // ------------------------------- //
    // dmer->numFinalSolutions = 2000; // WARNING: it is optional but Dakota shows and error if not specified.
    // ---------------------- //

    setDakotaMethodArgs(method.getArgs(),dmer);

    // -------------------------------------- //
    // Input estimation project configuration //
    // -------------------------------------- //
    if (prj->getType() == ptInputEst)
    {
        dmer->initializationType = DAKOTA_FLAT_FILE.toStdString();
        dmer->flatFile = FILE_INIT_POPULATION.toStdString();
    }

    return dme;
}

void argMathcing(QString name, QVariant value, Dakota::DataMethodRep *dmer)
{
    //qDebug() << name << " = " << value;

    // Fitness type argument
    // ---------------------
    if (name == "fitness_type"){ dmer->fitnessType = value.toString().toStdString(); return;}

    // Replacement type
    // ----------------
    if (name == "below_limit")       {dmer->fitnessLimit = value.toDouble();                  return;}
    if (name == "shrinkage_fraction"){dmer->shrinkagePercent = value.toDouble();              return;}
    if (name == "replacement_type")  {dmer->replacementType = value.toString().toStdString(); return;}

    // Convergence type (metric_tracker)
    // ----------------
    if (name == "metric_tracker")  {dmer->convergenceType = name.toStdString(); return;}
    if (name == "percent_change")  {dmer->percentChange = value.toDouble();     return;}
    if (name == "num_generations") {dmer->numGenerations = value.toInt();       return;}

    // Postprocessor type
    // ------------------
    if (name == "postprocessor_type") {dmer->postProcessorType = value.toString().toStdString(); return;}

    // Max iterations, function evaluations, population size
    // -----------------------------------------------------
    if (name == "max_iterations")           {dmer->maxIterations = value.toInt();          return;}
    if (name == "max_function_evaluations") {dmer->maxFunctionEvaluations = value.toInt(); return;}
    if (name == "population_size")          {dmer->populationSize = value.toInt();         return;}

    // Log file
    // --------
    if (name == "log_file") {dmer->logFile = value.toString().toStdString(); return;}

    // Print each population
    // ---------------------
    if (name == "print_each_pop") {dmer->printPopFlag = true; return;}

    // Initialization type
    // -------------------
    if (name == "initialization_type") {dmer->initializationType = value.toString().toStdString(); return;}
    if (name == "flat_file")           {dmer->flatFile = value.toString().toStdString(); return;}

    // Crossover type
    // --------------
    if (name == "crossover_type")
        {dmer->crossoverType = value.toString().toStdString(); return;}
    if (name == "multi_point_binary" ||
        name == "multi_point_parameterized_binary" ||
        name == "multi_point_real")
        {dmer->numCrossPoints = value.toInt();   return;}
    if (name == "num_parents")      {dmer->numParents = value.toInt();       return;}
    if (name == "num_offspring")    {dmer->numOffspring = value.toInt();     return;}
    if (name == "crossover_rate")   {dmer->crossoverRate = value.toDouble(); return;}

    // Mutation type
    // -------------
    if (name == "mutation_type")  {dmer->mutationType  = value.toString().toStdString(); return;}
    if (name == "mutation_scale") {dmer->mutationScale = value.toDouble();               return;}
    if (name == "mutation_rate")  {dmer->mutationRate  = value.toDouble();               return;}

    // Seed
    // ----
    if (name == "seed") {dmer->randomSeed = value.toInt(); return;}

    // Convergence tolerance
    // ---------------------
    if (name == "convergence_tolerance")
    {
        dmer->convergenceTolerance = value.toDouble(); return;
    }

    // Missing argument
    // ----------------
    qDebug() << "Method argument not assigned: " << name << ", " << value;
}

void argKeyword(opt_method_arg_keyword *arg, Dakota::DataMethodRep *dmer)
{
    argMathcing(arg->getId(),0,dmer);
}

void argReal(opt_method_arg_real *arg, Dakota::DataMethodRep *dmer)
{
    argMathcing(arg->getId(),arg->getValue(),dmer);
}

void argInt(opt_method_arg_integer *arg, Dakota::DataMethodRep *dmer)
{
    argMathcing(arg->getId(),arg->getValue(),dmer);
}

void argFile(opt_method_arg_filename *arg, Dakota::DataMethodRep *dmer)
{
    argMathcing(arg->getId(),arg->getValue(),dmer);
}

void argSubSet(opt_method_arg_set *arg, Dakota::DataMethodRep *dmer)
{
    bool found = false;
    int  i     = 0;

    argMathcing(arg->getId(),arg->getValue(),dmer);

    while (!found && i<arg->getOptions().count())
    {
        found = arg->getValue() == arg->getOptions()[i]->getId();
        if (!found) i++;
    }

    if (found && arg->getOptions()[i]->type()!=arg_keyword)
    {
        QList<opt_method_arg *> args;

        args.append(arg->getOptions()[i]);
        setDakotaMethodArgs(args,dmer);
    }
}

void argStruct(opt_method_arg_struct *arg, Dakota::DataMethodRep *dmer)
{
    setDakotaMethodArgs(arg->getArgs(),dmer);
}


void setDakotaMethodArgs(QList<opt_method_arg *> args, Dakota::DataMethodRep *dmer)
{
    for(int i=0;i<args.count();i++)
    {
        if (args[i]->getChecked() || !args[i]->getOptional())
        {
            switch(args[i]->type())
            {
                case arg_keyword:  argKeyword((opt_method_arg_keyword *)args[i],dmer); break;
                case arg_real:     argReal((opt_method_arg_real *)args[i],dmer);       break;
                case arg_integer:  argInt((opt_method_arg_integer *)args[i],dmer);     break;
                case arg_filename: argFile((opt_method_arg_filename *)args[i],dmer);   break;
                case arg_subset:   argSubSet((opt_method_arg_set *)args[i],dmer);      break;
                case arg_struct:   argStruct((opt_method_arg_struct *)args[i],dmer);   break;
                case arg_string:   break; // Not used
                case arg_bool:     break; // Not used
            }
        }
    }
}

DataModel setDakotaModel()
{
    Dakota::DataModel dmo;
    Dakota::DataModelRep *dmor;

    dmor            = dmo.data_rep();
    dmor->idModel   = sEmpty.toStdString();

    return dmo;
}

Dakota::DataVariables setDakotaVariables(opt_project *prj)
{
    Dakota::DataVariables     dv;
    Dakota::DataVariablesRep *dvr;
    QList<opt_parameter>      vars = prj->getPrb().getParameters();
    QList<opt_const_eq>       ceq  = prj->getPrb().getLinear_eq();
    QList<opt_const_ie>       cie  = prj->getPrb().getLinear_ie();

    StringArray          varNames;
    StringArray          varScalingT;
    RealVector           varValues(vars.count());
    RealVector           varLowBound(vars.count());
    RealVector           varUppBound(vars.count());
    RealVector           varScales(vars.count());
    RealVector           lecCoeficient(vars.count()*ceq.count());
    RealVector           lecTarget(ceq.count());
    StringArray          lecScalingT;
    RealVector           lecScales(ceq.count());
    RealVector           lieCoeficient(vars.count()*cie.count());
    RealVector           lieLower(cie.count());
    RealVector           lieUpper(cie.count());
    StringArray          lieScalingT;
    RealVector           lieScales(cie.count());

    for(int i=0;i<vars.count();i++)
    {
        varNames.push_back(vars[i].getDescriptor().toStdString());
        varScalingT.push_back(getScalingTypeText(vars[i].getScalingType()).toStdString());
        varValues(i)   = vars[i].getInitialValue();
        varLowBound(i) = vars[i].getLowerBound();
        varUppBound(i) = vars[i].getUpperBound();
        varScales(i)   = vars[i].getScalingValue();
    }

    for(int i=0;i<ceq.count();i++)
    {
        lecTarget(i)   = ceq[i].b;
        lecScales(i)   = ceq[i].scv;
        lecScalingT.push_back(getScalingTypeText(ceq[i].sct).toStdString());
        for(int j=0;j<vars.count();j++)
            lecCoeficient(i+j*vars.count()) = ceq[i].a[j];
    }

    for(int i=0;i<cie.count();i++)
    {
        lieLower(i)  = cie[i].has_lb ? cie[i].lb : -DBL_MAX; // When no c++11 suport
                                                   //std::numeric_limits<double>::lowest(); // < -BIG_REAL_BOUND
        lieUpper(i)  = cie[i].has_ub ? cie[i].ub : std::numeric_limits<double>::max();    // > +BIG_REAL_BOUND
        lieScales(i) = cie[i].scv;
        lieScalingT.push_back(getScalingTypeText(cie[i].sct).toStdString());
        for(int j=0;j<vars.count();j++)
            lieCoeficient(i+j*vars.count()) = cie[i].a[j];
    }

    dvr                             = dv.data_rep();
    dvr->numContinuousDesVars       = vars.count();
    dvr->continuousDesignLabels     = varNames;
    dvr->continuousDesignVars       = varValues;
    dvr->continuousDesignLowerBnds  = varLowBound;
    dvr->continuousDesignUpperBnds  = varUppBound;
    dvr->continuousDesignScaleTypes = varScalingT;
    dvr->continuousDesignScales     = varScales;
    dvr->linearEqConstraintCoeffs   = lecCoeficient;
    dvr->linearEqTargets            = lecTarget;
    dvr->linearEqScaleTypes         = lecScalingT;
    dvr->linearEqScales             = lecScales;
    dvr->linearIneqConstraintCoeffs = lieCoeficient;
    dvr->linearIneqLowerBnds        = lieLower;
    dvr->linearIneqUpperBnds        = lieUpper;
    dvr->linearIneqScaleTypes       = lieScalingT;
    dvr->linearIneqScales           = lieScales;

    return dv;
}

Dakota::DataInterface setDakotaInterface()
{
    Dakota::DataInterface     di;
    Dakota::DataInterfaceRep *dir;

    dir = di.data_rep();

    // FMU interface
    dir->interfaceType   = Dakota::TEST_INTERFACE;
    dir->analysisDrivers.push_back(DAKOTA_FMU_PLUGIN.toStdString());

    // Restart optimization
    dir->evalCacheFlag   = false;
    dir->restartFileFlag = false;

    // Parallelization
    // dir->interfaceSynchronization   = Dakota::ASYNCHRONOUS_INTERFACE;
    // dir->asynchLocalEvalConcurrency = 4;
    // dir->asynchLocalEvalScheduling  = Dakota::STATIC_SCHEDULING;

    return di;
}

Dakota::DataResponses setDakotaResponses(opt_project *prj)
{
    Dakota::DataResponses     dr;
    Dakota::DataResponsesRep *drr;
    QList<variable>           outs = prj->getModel().outputs;
    QList<opt_objfnc>         objs = prj->getPrb().getObjFncs();
    QList<opt_const_eq>       ceq  = prj->getPrb().getNon_linear_eq();
    QList<opt_const_ie>       cie  = prj->getPrb().getNon_linear_ie();

    StringArray       resNames;
    StringArray       objCriteria;
    StringArray       objScalingT;
    RealVector        objWeights(objs.count());
    RealVector        objScales(objs.count());
    RealVector        nieLower(cie.count());
    RealVector        nieUpper(cie.count());
    StringArray       nieScalingT;
    RealVector        nieScales(cie.count());
    RealVector        neqTarget(ceq.count());
    StringArray       neqScalingT;
    RealVector        neqScales(ceq.count());

    drr = dr.data_rep();

    // Objective functions
    for(int i=0;i<objs.count();i++)
    {
        resNames.push_back(objFormatedName(objs[i]));
        objCriteria.push_back(objs[i].getCriteriaText().toStdString());
        objScalingT.push_back(getScalingTypeText(objs[i].getScalingType()).toStdString());
        objScales(i)  = objs[i].getScalingValue();
        objWeights(i) = objs[i].getWeight();
    }
    drr->numObjectiveFunctions       = objs.count();
    drr->primaryRespFnSense          = objCriteria;
    drr->primaryRespFnScaleTypes     = objScalingT;
    drr->primaryRespFnScales         = objScales;
    drr->gradientType                = DAKOTA_NONE.toStdString();
    drr->hessianType                 = DAKOTA_NONE.toStdString();
    if (objs.count()>0 && objs[0].getUseWeight())
        drr->primaryRespFnWeights    = objWeights;

    // Nonlinear inequality constraints
    for(int i=0;i<cie.count();i++)
    {
        resNames.push_back(cieFormatedName(cie[i],outs));
        nieLower(i) = cie[i].has_lb ? cie[i].lb : -DBL_MAX; // When no c++11 suport
                                                  //std::numeric_limits<double>::lowest(); // < -BIG_REAL_BOUND
        nieUpper(i) = cie[i].has_ub ? cie[i].ub : std::numeric_limits<double>::max();    // > +BIG_REAL_BOUND
        nieScalingT.push_back(getScalingTypeText(cie[i].sct).toStdString());
        nieScales(i) = cie[i].scv;
    }
    drr->numNonlinearIneqConstraints       = cie.count();
    drr->nonlinearIneqLowerBnds            = nieLower;
    drr->nonlinearIneqUpperBnds            = nieUpper;
    drr->nonlinearIneqScaleTypes           = nieScalingT;
    drr->nonlinearIneqScales               = nieScales;

    // Nonlinear equality constraints
    for(int i=0;i<ceq.count();i++)
    {
        resNames.push_back(ceqFormatedName(ceq[i],outs));
        neqScalingT.push_back(getScalingTypeText(ceq[i].sct).toStdString());
        neqScales(i) = ceq[i].scv;
        neqTarget(i) = ceq[i].b;
    }
    drr->numNonlinearEqConstraints   = ceq.count();
    drr->nonlinearEqTargets          = neqTarget;
    drr->nonlinearEqScaleTypes       = neqScalingT;
    drr->nonlinearEqScales           = neqScales;

    // Responses
    drr->numResponseFunctions = objs.count() + cie.count() + ceq.count();
    drr->responseLabels       = resNames;

    return dr;
}

void setFileOutError(QString &fileOut, QString &fileError)
{
    fileOut   = QDir::currentPath() + QDir::separator() + DAKOTA_FILE_OUT;
    fileError = QDir::currentPath() + QDir::separator() + DAKOTA_FILE_ERROR;
}

SIM::FMUInterface *serial_interface_plugin(opt_project *prj, Dakota::LibraryEnvironment &env)
{
  bool plugged_in;

  Dakota::ProblemDescDB &problem_db   = env.problem_description_db();
  SIM::FMUInterface     *FMU_int      = new SIM::FMUInterface(problem_db,prj);
  Dakota::Interface     *serial_iface = FMU_int;

  plugged_in = env.plugin_interface(model_type, interf_type, an_driver, serial_iface);

  if (!plugged_in)
  {
    Cerr << "Error: no serial interface plugin performed. Check "
         << "compatibility between parallel\n configuration and "
         << "selected analysis_driver." << std::endl;
    Dakota::abort_handler(-1);
  }
  return FMU_int;
}

//SIM::FMUInterface *parallel_interface_plugin(Dakota::LibraryEnvironment& env)
//{
//  Dakota::ModelList filt_models = env.filtered_model_list(model_type, interf_type, an_driver);

//  if (filt_models.empty())
//  {
//    Cerr << "Error: no parallel interface plugin performed.  Check "
//         << "compatibility between parallel\n       configuration and "
//         << "selected analysis_driver." << std::endl;
//    Dakota::abort_handler(-1);
//  }

//  Dakota::ProblemDescDB& problem_db = env.problem_description_db();
//  Dakota::ModelLIter ml_iter;
//  size_t model_index = problem_db.get_db_model_node(); // for restoration
//  for (ml_iter = filt_models.begin(); ml_iter != filt_models.end(); ++ml_iter)
//  {
//    // set DB nodes to input specification for this Model
//    problem_db.set_db_model_nodes(ml_iter->model_id());

//    Dakota::Interface& model_interface = ml_iter->derived_interface();

//    // Parallel case: plug in derived Interface object with an analysisComm.
//    // Note: retrieval and passing of analysisComm is necessary only if
//    // parallel operations will be performed in the derived constructor.

//    // retrieve the currently active analysisComm from the Model.  In the most
//    // general case, need an array of Comms to cover all Model configurations.
//    const MPI_Comm& analysis_comm = ml_iter->analysis_comm();

//    // don't increment ref count since no other envelope shares this letter
//    model_interface.assign_rep(new
//      SIM::ParallelDirectApplicInterface(problem_db, analysis_comm), false);
//  }
//  problem_db.set_db_model_nodes(model_index); // restore
//}

void deleteProject(opt_project **prj)
{
    if (*prj != NULL)
    {
        delete *prj;
        *prj = NULL;
    }
}

void deleteDakotaProblemDB(Dakota::LibraryEnvironment **env)
{
    if (*env != NULL)
    {
        delete *env;
        *env = NULL;
    }
}

void deleteFMUInterface(SIM::FMUInterface **FMU_int)
{
    if (*FMU_int != NULL)
    {
        delete *FMU_int;
        *FMU_int = NULL;
    }
}

void executeDakota(Dakota::LibraryEnvironment &env)
{
    // Execute the environment
    try{
        env.execute();
    }catch(...){}
}

void getResults(opt_project *prj, LibraryEnvironment env, Dakota::ProblemDescDB &pdb,
                QList<opt_result> &pars, QList<opt_result> &objs, QList<opt_result> &cons)
{
    opt_problem          prb         = prj->getPrb();
    int                  objFunCount = prb.getObjFncs().count();
    Variables            vars;
    Response             resp;

    // Multiple points
    Dakota::IteratorList ili = pdb.iterator_list();
    Dakota::Iterator     ite = *(ili.begin());

    // Results: parameters
    for(int i=0;i<prb.getParameters().count();i++)
    {
        opt_result    par;
        QList<double> values;

        // Parameter values
        if (ili.size()>0 && ite.returns_multiple_points())
        {
            VariablesArray varArray = ite.variables_array_results();

            if (varArray.size()>0)
                par.setName(QString::fromStdString(varArray[0].continuous_variable_labels()[i]));
            for (unsigned j=0;j<varArray.size();j++)
            {
                vars = varArray[j];
                values.append(vars.continuous_variable(i));
            }
        }
        else
        {
            vars = env.variables_results();
            par.setName(QString::fromStdString(vars.continuous_variable_labels()[i]));
            values.append(vars.continuous_variable(i));
        }
        // Add parameter values
        par.setValues(values);
        // Parameter info
        par.setDesc(sEmpty);
        par.setUnit(sEmpty);
        par.setType(FMI_REAL);
        // Add to parameter list
        pars.append(par);
    }

    // Results: objectives
    for(int i=0;i<objFunCount;i++)
    {
        opt_result    obj;
        QList<double> values;

        // Objective values
        if (ili.size()>0 && ite.returns_multiple_points())
        {
            ResponseArray respArray = ite.response_array_results();

            if (respArray.size()>0)
                obj.setName(QString::fromStdString(respArray[0].function_labels()[i]));
            for (unsigned j=0;j<respArray.size();j++)
            {
                resp = respArray[j];
                values.append(resp.function_value(i));
            }
        }
        else
        {
            resp = env.response_results();
            obj.setName(QString::fromStdString(resp.function_labels()[i]));
            values.append(resp.function_value(i));
        }
        // Add objective values
        obj.setValues(values);
        // Objective info
        obj.setDesc(sEmpty);
        obj.setUnit(sEmpty);
        obj.setType(FMI_REAL);
        // Add to objective list
        objs.append(obj);
    }

    // Results: constraints
    for(int i=objFunCount;i<objFunCount+prb.getNon_linear_eq().count()+prb.getNon_linear_ie().count();i++)
    {
        opt_result    con;
        QList<double> values;

        // Constraint values
        if (ili.size()>0 && ite.returns_multiple_points())
        {
            ResponseArray respArray = ite.response_array_results();

            if (respArray.size()>0)
                con.setName(QString::fromStdString(respArray[0].function_labels()[i]));
            for (unsigned j=0;j<respArray.size();j++)
            {
                resp = respArray[j];
                values.append(resp.function_value(i));
            }
        }
        else
        {
            resp = env.response_results();
            con.setName(QString::fromStdString(resp.function_labels()[i]));
            values.append(resp.function_value(i));
        }
        // Add objective values
        con.setValues(values);
        // Objective info
        con.setDesc(sEmpty);
        con.setUnit(sEmpty);
        con.setType(FMI_REAL);
        // Add to objective list
        cons.append(con);
    }
}

void saveResults(opt_project **prj, LibraryEnvironment env, Dakota::ProblemDescDB &pdb)
{
    opt_problem       prb = (*prj)->getPrb();
    QList<opt_result> pars;
    QList<opt_result> objs;
    QList<opt_result> cons;

    // Get results from ProblemDescDB or LibraryEnvironment
    getResults(*prj,env,pdb,pars,objs,cons);

    // Set data in problem
    prb.setRes_par(pars);
    prb.setRes_obj(objs);
    prb.setRes_con(cons);

    // Set problem in project
    (*prj)->setPrb(prb);
}


//void saveResults(opt_project **prj, LibraryEnvironment env, Dakota::ProblemDescDB &pdb)
//{
//    opt_problem          prb         = (*prj)->getPrb();
//    int                  objFunCount = prb.getObjFncs().count();
//    QList<opt_result>    pars;
//    QList<opt_result>    objs;
//    QList<opt_result>    cons;
//    Variables            vars;
//    Response             resp;

//    // Multiple points
//    Dakota::IteratorList ili = pdb.iterator_list();
//    Dakota::Iterator     ite = *(ili.begin());

//    // Results: parameters
//    for(int i=0;i<prb.getParameters().count();i++)
//    {
//        opt_result    par;
//        QList<double> values;

//        // Parameter values
//        if (ili.size()>0 && ite.returns_multiple_points())
//        {
//            VariablesArray varArray = ite.variables_array_results();

//            if (varArray.size()>0)
//                par.setName(QString::fromStdString(varArray[0].continuous_variable_labels()[i]));
//            for (unsigned j=0;j<varArray.size();j++)
//            {
//                vars = varArray[j];
//                values.append(vars.continuous_variable(i));
//            }
//        }
//        else
//        {
//            vars = env.variables_results();
//            par.setName(QString::fromStdString(vars.continuous_variable_labels()[i]));
//            values.append(vars.continuous_variable(i));
//        }
//        // Add parameter values
//        par.setValues(values);
//        // Parameter info
//        par.setDesc(sEmpty);
//        par.setUnit(sEmpty);
//        par.setType(FMI_REAL);
//        // Add to parameter list
//        pars.append(par);
//    }

//    // Results: objectives
//    for(int i=0;i<objFunCount;i++)
//    {
//        opt_result    obj;
//        QList<double> values;

//        // Objective values
//        if (ili.size()>0 && ite.returns_multiple_points())
//        {
//            ResponseArray respArray = ite.response_array_results();

//            if (respArray.size()>0)
//                obj.setName(QString::fromStdString(respArray[0].function_labels()[i]));
//            for (unsigned j=0;j<respArray.size();j++)
//            {
//                resp = respArray[j];
//                values.append(resp.function_value(i));
//            }
//        }
//        else
//        {
//            resp = env.response_results();
//            obj.setName(QString::fromStdString(resp.function_labels()[i]));
//            values.append(resp.function_value(i));
//        }
//        // Add objective values
//        obj.setValues(values);
//        // Objective info
//        obj.setDesc(sEmpty);
//        obj.setUnit(sEmpty);
//        obj.setType(FMI_REAL);
//        // Add to objective list
//        objs.append(obj);
//    }

//    // Results: constraints
//    for(int i=objFunCount;i<objFunCount+prb.getNon_linear_eq().count()+prb.getNon_linear_ie().count();i++)
//    {
//        opt_result    con;
//        QList<double> values;

//        // Constraint values
//        if (ili.size()>0 && ite.returns_multiple_points())
//        {
//            ResponseArray respArray = ite.response_array_results();

//            if (respArray.size()>0)
//                con.setName(QString::fromStdString(respArray[0].function_labels()[i]));
//            for (unsigned j=0;j<respArray.size();j++)
//            {
//                resp = respArray[j];
//                values.append(resp.function_value(i));
//            }
//        }
//        else
//        {
//            resp = env.response_results();
//            con.setName(QString::fromStdString(resp.function_labels()[i]));
//            values.append(resp.function_value(i));
//        }
//        // Add objective values
//        con.setValues(values);
//        // Objective info
//        con.setDesc(sEmpty);
//        con.setUnit(sEmpty);
//        con.setType(FMI_REAL);
//        // Add to objective list
//        cons.append(con);
//    }

//    // Set data in problem
//    prb.setRes_par(pars);
//    prb.setRes_obj(objs);
//    prb.setRes_con(cons);

//    // Set problem in project
//    (*prj)->setPrb(prb);
//}
