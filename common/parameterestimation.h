#ifndef PARAMETERESTIMATION_H
#define PARAMETERESTIMATION_H

#include <QtCore>

#include "dakota/fmuinterface.h"
#include "opt_project.h"
#include "common.h"

class parameterEstimation: public QObject
{
    Q_OBJECT

private:
    // Variables -------------------
    opt_project *prj;
    int          t_pe;
    fmiTime      time;
    QString      fileOut;
    QString      fileError;
    QString      dakota_log_level;

public:
    parameterEstimation(opt_project *p);
    ~parameterEstimation();
    int getTime();
    opt_project *getProject();
    QString getOutFile();
    QString getErrorFile();
    void requestStop();

    QString getDakota_log_level() const;
    void setDakota_log_level(const QString &value);

public slots:
    void run();
    void update();

signals:
    void finished(double t, opt_exp exp, opt_problem prb, unsigned status);
    void end();
    void parEstUpdate(double t, double tstart, double tstop);
    void parEstError(unsigned s, QString msg);
    void stop();
};

#endif // PARAMETERESTIMATION_H
