#include "common.h"

#include <JlCompress.h>

#include "formats/read_csv.h"
#include "formats/read_matlab4.h"
#include <QImage>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <QDebug>

void deleteArgs(QList<opt_method_arg *> args)
{
    // WARNING
    // To avoid memory leaks each class must
    // be explicitly deleted

    for(int i=0;i<args.count();i++)
    {
        if (args[i]!=NULL)
        {
            switch(args[i]->type())
            {
                case arg_keyword:
                {
                    opt_method_arg_keyword *a = (opt_method_arg_keyword *)args[i];
                    delete a;
                    break;
                }
                case arg_real:
                {
                    opt_method_arg_real *a = (opt_method_arg_real *)args[i];
                    delete a;
                    break;
                }
                case arg_integer:
                {
                    opt_method_arg_integer *a = (opt_method_arg_integer *)args[i];
                    delete a;
                    break;
                }
                case arg_string:
                {
                    opt_method_arg_string *a = (opt_method_arg_string *)args[i];
                    delete a;
                    break;
                }
                case arg_filename:
                {
                    opt_method_arg_filename *a = (opt_method_arg_filename *)args[i];
                    delete a;
                    break;
                }
                case arg_subset:
                {
                    opt_method_arg_set *a = (opt_method_arg_set *)args[i];
                    deleteArgs(a->getOptions());
                    delete a;
                    a = NULL;
                    break;
                }
                case arg_struct:
                {
                    opt_method_arg_struct *a = (opt_method_arg_struct *)args[i];
                    deleteArgs(a->getArgs());
                    delete a;
                    a = NULL;
                    break;
                }
            }
        }
    }
    args.clear();
}

void copyArgs(QList<opt_method_arg *> &des, const QList<opt_method_arg *> src)
{
    bool freeSource = false;
    QList<opt_method_arg *> source;

    if (des==src)
    {
        copyArgs(source,src);
        freeSource = true;
    }
    else
        source = src;

    qDeleteAll(des);
    des.clear();

    for(int i=0;i<src.count();i++)
    {
            switch(src[i]->type())
            {
                case arg_keyword:
                {
                                opt_method_arg_keyword *a = (opt_method_arg_keyword *)source[i];
                                des.append(new opt_method_arg_keyword(a->getArgument(),a->getId(),a->getDesc(),a->getOptional()));
                                des[i]->setChecked(a->getChecked());
                                break;
                }
                case arg_string:
                {
                                opt_method_arg_string *a = (opt_method_arg_string *)source[i];
                                des.append(new opt_method_arg_string(a->getArgument(),a->getId(),a->getDesc(),a->getOptional(),a->getValue(),a->getDef()));
                                des[i]->setChecked(a->getChecked());
                                break;
                }
                case arg_real:
                {
                                opt_method_arg_real *a = (opt_method_arg_real *)source[i];
                                des.append(new opt_method_arg_real(a->getArgument(),a->getId(),a->getDesc(),a->getOptional(),a->getValue(),a->getDef(),a->getMin(),a->getMax()));
                                des[i]->setChecked(a->getChecked());
                                break;
                }
                case arg_integer:
                {
                                opt_method_arg_integer *a = (opt_method_arg_integer *)source[i];
                                des.append(new opt_method_arg_integer(a->getArgument(),a->getId(),a->getDesc(),a->getOptional(),a->getValue(),a->getDef(),a->getMin(),a->getMax()));
                                des[i]->setChecked(a->getChecked());
                                break;
                }
                case arg_filename:
                {
                                opt_method_arg_filename *a = (opt_method_arg_filename *)source[i];
                                des.append(new opt_method_arg_filename(a->getArgument(),a->getId(),a->getDesc(),a->getOptional(),a->getValue(),a->getDef(),a->getOpen()));
                                des[i]->setChecked(a->getChecked());
                                break;
                }
                case arg_bool:
                {
                                opt_method_arg_bool *a = (opt_method_arg_bool *)source[i];
                                des.append(new opt_method_arg_bool(a->getArgument(),a->getId(),a->getDesc(),a->getOptional(),a->getValue(),a->getDef()));
                                des[i]->setChecked(a->getChecked());
                                break;
                }
                case arg_subset:
                {
                                QList<opt_method_arg *> options;

                                opt_method_arg_set *a = (opt_method_arg_set *)source[i];
                                copyArgs(options,a->getOptions());
                                des.append(new opt_method_arg_set(a->getArgument(),a->getId(),a->getDesc(),a->getOptional(),a->getValue(),a->getDef(),options));
                                des[i]->setChecked(a->getChecked());
                                break;
                }
                case arg_struct:
                {
                                QList<opt_method_arg *> args;

                                opt_method_arg_struct *a = (opt_method_arg_struct *)source[i];
                                copyArgs(args,a->getArgs());
                                des.append(new opt_method_arg_struct(a->getArgument(),a->getId(),a->getDesc(),a->getOptional(),args));
                                des[i]->setChecked(a->getChecked());
                                break;
                }
            }
    }

    if (freeSource)
    {
        qDeleteAll(source);
        source.clear();
    }
}

int searchVar(QList<variable> vars, QString name)
{
    for(int i=0;i<vars.size();i++)
    {
        if (vars[i].name == name) return i;
    }
    return -1;
}

//template <typename T>
//unsigned getDataCSVfile(QString file, QStringList names, QList<QList<T> > &values, QString &error)
//{
//    struct csv_data *csv_reader;

//    error        = sEmpty;
//    csv_reader   = read_csv(file.toStdString().c_str());
//    if (csv_reader != NULL)
//    {
//        values.clear();
//        for(int i=0;i<names.count();i++)
//        {
//            double *csv_vals;
//            QList<double> vals;

//            csv_vals = read_csv_dataset(csv_reader,names[i].toStdString().c_str());
//            if (csv_vals == NULL) return FILE_NO_VAR;

//            for(int j=0;j<csv_reader->numsteps;j++)
//                vals.append(csv_vals[j]);

//            values.append(vals);
//        }
//        omc_free_csv_reader(csv_reader);
//        return FILE_OK;
//    }
//    return FILE_WRONG_FORMAT;
//}

//template <typename T>
//unsigned getDataMATfile(QString file, QStringList names, QList<QList<T> > &values, QString &error)
//{
//    ModelicaMatReader mat;

//    error = omc_new_matlab4_reader(file.toStdString().c_str(),&mat);
//    if (error == 0)
//    {
//        values.clear();
//        for(int i=0;i<names.count();i++)
//        {
//            ModelicaMatVariable_t *mvar;
//            double *mat_vals;
//            QList<double> vals;

//            mvar = omc_matlab4_find_var(&mat,names[i].toStdString().c_str());
//            if (mvar == NULL) return FILE_NO_VAR;

//            mat_vals = omc_matlab4_read_vals(&mat,mvar->index);
//            if (mat_vals == NULL) return FILE_NO_VAR;

//            for(unsigned int j=0;j<mat.nrows;j++)
//                vals.append(mat_vals[j]);

//            values.append(vals);
//        }
//        omc_free_matlab4_reader(&mat);
//        return FILE_OK;
//    }
//    return FILE_WRONG_FORMAT;
//}

//template <typename T>
//unsigned getDataFromFile(QString file, unsigned format, QStringList names, QList<QList<T> > &values, QString &error)
//{
//    // Check file exist and supported format
//    unsigned res = checkFileExistAndSupported(file);
//    if (res!=FILE_OK) return res;

//    // Check file format
//    if (format == ffCSV)
//        return getDataCSVfile(file,names,values,error);
//    else if (format == ffTRJ)
//        return getDataMATfile(file,names,values,error);
//    else return FILE_UNSUPPORTED;
//}

unsigned checkFileExistAndSupported(QString file)
{
    QFileInfo   fileInfo(file);
    QStringList supportedFiles;

    // Exist file
    if (!fileInfo.exists()) return FILE_NO_EXIST;

    // Supported file
    QString ext = fileInfo.suffix();
    supportedFiles.append(eMAT);
    supportedFiles.append(eTXT);
    supportedFiles.append(eCSV);
    if (supportedFiles.indexOf(ext)<0) return FILE_UNSUPPORTED;

    return FILE_OK;
}

unsigned checkFileExistAndSupported(QString file, QString &ext)
{
    QFileInfo   fileInfo(file);
    QStringList supportedFiles;

    // Exist file
    if (!fileInfo.exists()) return FILE_NO_EXIST;

    // Supported file
    ext = fileInfo.suffix();
    supportedFiles.append(eMAT);
    supportedFiles.append(eTXT);
    supportedFiles.append(eCSV);
    if (supportedFiles.indexOf(ext)<0) return FILE_UNSUPPORTED;

    return FILE_OK;
}

QDir projectQDir(opt_project *prj)
{
    QFileInfo fpathInfo(prj->getFilename());
    QDir      path(prj->getFilename().isEmpty() ? QDir::currentPath() : fpathInfo.absoluteDir());
    return    path;
}

QString projectPath(opt_project *prj)
{
    return projectQDir(prj).absolutePath();
}

QString fileRelativeToProject(opt_project *prj, QString filename)
{
    return filename.isEmpty() ? filename : projectQDir(prj).relativeFilePath(filename);
}

QString fileAbsoluteFromProject(opt_project *prj, QString filename)
{
    return filename.isEmpty() ? filename : projectPath(prj) + QDir::separator() + filename;
}

void setModelVarType(fmi_2_0::FMUModelExchange &fmu, unsigned type, QString name, QString value)
{
    switch(type)
    {
        case FMI_REAL:
            fmu.setValue(name.toStdString(),value.toDouble());
            break;
        case FMI_INTEGER:
            fmu.setValue(name.toStdString(),value.toInt());
            break;
        case FMI_BOOL:
            fmu.setValue(name.toStdString(),value.toStdString().c_str()[0]);
            break;
        case FMI_STRING:
            fmu.setValue(name.toStdString(),value.toStdString());
            break;
        default:
            break;
    }
}

void setModelParameters(fmi_2_0::FMUModelExchange &fmu, opt_model mo)
{
    int pos;

    for(int i=0;i<mo.p_name_new.count();i++)
    {
        pos = mo.findParam(mo.p_name_new[i]);
        if (pos>=0)
        {
            setModelVarType(fmu,mo.params[pos].type,mo.p_name_new[i],mo.p_value_new[i]);
            //qDebug() << "Set param - " << mo.p_name_new[i] << " = " << mo.p_value_new[i];
        }

        pos = mo.findInput(mo.p_name_new[i]);
        if (pos>=0)
        {
            setModelVarType(fmu,mo.inputs[pos].type,mo.p_name_new[i],mo.p_value_new[i]);
            //qDebug() << "Set input - " << mo.p_name_new[i] << " = " << mo.p_value_new[i];
        }
    }
}

void setModelInputs(fmi_2_0::FMUModelExchange &fmu, opt_model mo, opt_exp exp, fmi2Real time)
{
    // WARNING: OMP does not improve speed, it depends on the number of inputs
    // #pragma omp parallel for

    // Real inputs
    if (exp.getiRealCount()>0)
    {
        fmi2Real    *pd;
        std::string *names = exp.getiRealNames();

        pd = exp.getiRealVals(time);
        for(unsigned i=0;i<exp.getiRealCount();i++)
        {
            if (mo.p_name_new.indexOf(QString::fromStdString(names[i]))<0)
            {
                fmu.setValue(names[i],pd[i]);
                //qDebug() << "Time:" << time << names[i].c_str() << " = " << pd[i];
            }
        }
        delete[] pd;
        delete[] names;
    }
    // Integer inputs
    if (exp.getiIntCount()>0)
    {
        fmi2Integer *pi = new fmi2Integer[exp.getiIntCount()];

        pi = exp.getiIntVals(time);
        for(unsigned i=0;i<exp.getiIntCount();i++)
        {
            fmu.setValue(exp.getiIntNames()[i],pi[i]);
            //qDebug() << "Time:" << time << exp.getiIntNames()[i].c_str() << " = " << pi[i];
        }
        delete[] pi;
    }
    // Boolean inputs
    if (exp.getiBoolCount()>0)
    {
        fmi2Boolean *pb = new fmi2Boolean[exp.getiBoolCount()];

        pb = exp.getiBoolVals(time);
        for(unsigned i=0;i<exp.getiBoolCount();i++)
            fmu.setValue(exp.getiBoolNames()[i],pb[i]);
        delete pb;
    }
    // String inputs
    if (exp.getiStringCount()>0)
    {
        fmi2String *ps = new fmi2String[exp.getiStringCount()];

        ps = exp.getiStringVals(time);
        for(unsigned i=0;i<exp.getiStringCount();i++)
            fmu.setValue(exp.getiStringNames()[i],ps[i]);
        delete ps;
    }
}

QString msToTime(int ms)
{
   QString mseconds = QString::number((ms%1000)/100);
   QString seconds  = QString::number((ms/1000)%60);
   QString minutes  = QString::number((ms/(1000*60))%60);
   QString hours    = QString::number((ms/(1000*60*60))%24);

   hours    = (hours.toInt()    < 10)  ? "0"  + hours    : hours;
   minutes  = (minutes.toInt()  < 10)  ? "0"  + minutes  : minutes;
   seconds  = (seconds.toInt()  < 10)  ? "0"  + seconds  : seconds;
   mseconds = (mseconds.toInt() < 10)  ? "0"  + mseconds : mseconds;
   mseconds = (mseconds.toInt() < 100) ? "0"  + mseconds : mseconds;

   return hours + ":" + minutes + ":" + seconds + "." + mseconds;
}

void sendToLog(QStringList &log, QList<unsigned> &ltype, QStringList &ltime,
               QString l, unsigned type, QString time)
{
    log.append(l);
    ltype.append(type);
    ltime.append(time.isEmpty() ? QDate::currentDate().toString() + " " + QTime::currentTime().toString() : time);
}

int findSelItem(opt_const_eq l)
{
    bool found = false;
    int  i     = 0;

    while (!found && i<l.a.count())
    {
        found = l.a[i] != 0;
        if (!found) i++;
    }
    return found ? i : -1;
}

int findSelItem(opt_const_ie l)
{
    bool found = false;
    int  i     = 0;

    while (!found && i<l.a.count())
    {
        found = l.a[i] != 0;
        if (!found) i++;
    }
    return found ? i : -1;
}

std::string objFormatedName(opt_objfnc obj)
{
    return obj.getName().toStdString();
}

std::string ceqFormatedName(opt_const_eq c, QList<variable> outs)
{
    int     index = findSelItem(c);
    QString cad   = sEmpty;

    if (index>-1)
        cad = outs[index].name + sEqual + QString::number(c.b);

    return cad.toStdString();
}

std::string cieFormatedName(opt_const_ie c, QList<variable> outs)
{
    int     index = findSelItem(c);
    QString cad   = sEmpty;

    if (index>-1)
    {
        cad = c.has_lb ? QString::number(c.lb) + sLessEqual : cad;
        cad = cad + outs[index].name;
        cad = c.has_ub ? cad + sLessEqual + QString::number(c.ub) : cad;
    }
    return cad.toStdString();
}

QStringList extractFMUdir(opt_project *prj)
{
    // Temporary directory
    QTemporaryDir  dir;
    opt_model      mo;

    dir.setAutoRemove(false);
    mo = prj->getModel();
    mo.uri =  dir.path();
    prj->setModel(mo);
    return JlCompress::extractDir(fileAbsoluteFromProject(prj,prj->getModel().getFMUfileName()),dir.path());
}

void deleteDir(QString dir)
{
    if (!dir.isEmpty())
    {
        QDir tmpDir(dir);
        tmpDir.removeRecursively();
    }
}

QString findMOGALogFile(opt_project *p)
{
    bool found = false;
    int  i     = 0;
    QList<opt_method_arg *> args = p->getPrb().getMethod().getArgs();

    while (!found && (i<args.count()))
    {
        if (args[i]->type() == arg_filename)
        {
            opt_method_arg_filename *arg = (opt_method_arg_filename *)args[i];
            found = arg->getId() == "log_file";
            if (found) return arg->getValue();
        }
        i++;
    }
    return sEmpty;
}

int findVar(QString v, QList<variable> list)
{
    bool found = false;
    int  pos = 0;

    while (!found && pos<list.count())
    {
        found = list[pos].name == v;
        if (!found) pos++;
    }
    return found ? pos : -1;
}

inline
cv::Mat qimage_to_mat_ref(QImage &img, int format)
{
    return cv::Mat(img.height(), img.width(),
                   format, img.bits(), img.bytesPerLine());
}

cv::Mat qimage_to_mat_ref(QImage &img, bool swap)
{
    if(img.isNull()){
        return cv::Mat();
    }

    switch (img.format()) {
    case QImage::Format_RGB888:{
        auto result = qimage_to_mat_ref(img, CV_8UC3);
        if(swap){
            cv::cvtColor(result, result, CV_RGB2BGR);
        }
        return result;
    }
    case QImage::Format_Indexed8:{
        return qimage_to_mat_ref(img, CV_8U);
    }
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32:
    case QImage::Format_ARGB32_Premultiplied:{
        return qimage_to_mat_ref(img, CV_8UC4);
    }
    default:
        break;
    }

    return {};
}

cv::Mat qimage_to_mat_cpy(QImage const &img, bool swap)
{
    return qimage_to_mat_ref(const_cast<QImage&>(img), swap).clone();
}

// https://stackoverflow.com/questions/37861764/creating-gif-from-qimages-with-ffmpeg
bool imagesToVideo(QList<QImage> li, QString file, int framesPerSecond)
{
    // WARNING: Codecs ¿?
    // WARNING: Poor video quality

    try {

        if (li.count()>0)
        {
            QImage qImage = li[0];
            cv::VideoWriter video(file.toStdString().c_str(),CV_FOURCC('M','J','P','G'), framesPerSecond, cv::Size(qImage.width(),qImage.height()),true);

            if (video.isOpened())
            {
                for(int i=0;i<li.count();i++)
                {
                    // WARNING: little and big endian, 32 or 64 bits, Linux or Windows ¿?
                    qImage = li[i].rgbSwapped();

                    //Get QImage data to Open-cv Mat
                    cv::Mat frame = qimage_to_mat_cpy(qImage,false);

                    //cv::Mat(qImage.height(),qImage.width(),format,const_cast<uchar*>(qImage.bits()),qImage.bytesPerLine()).clone();

                    //Write frame to VideoWriter
                    video.write(frame);
                }
                video.release();
            }
        }
    }
    catch (QException &ex) {
        qDebug() << "Exception converting image to PNG format: " << ex.what();
        return false;
    }
    return true;
}
